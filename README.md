# README #

EDMS is an electronic document management system.

### Aim ###
The aim is the development of a public key infrastructure by example of an electronic document management system, ensuring information integrity, authenticity and irrefutability, detection of the most vulnerable aspect in the Internet application security field and the tracks of their solution through using PKI technology.

The method of design is Java platform, integrated development environment for developing computer software IntelliJ IDEA 14.0, Java programming language, Java Servlet technology.

As a result of the work developed an electronic document management system. In the exercise of electronic document management services such as information integrity, authenticity and irrefutability are ensured. The elliptic curve algorithm based on the standard DSTU 4145-2002 used as a basic algorithm for ensuring information integrity and authenticity.
This README would normally document whatever steps are necessary to get your application up and running.