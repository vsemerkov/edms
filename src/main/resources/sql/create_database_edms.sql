CREATE DATABASE IF NOT EXISTS `edms`;

USE `edms`;

DROP TABLE IF EXISTS `deferred_commands`;
DROP TABLE IF EXISTS `certificate_servers`;
DROP TABLE IF EXISTS `documents`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `params`;

CREATE TABLE `params` (
  `id` BIGINT(20)  NOT NULL AUTO_INCREMENT,
  `p`  DECIMAL(40) NOT NULL,
  `g`  DECIMAL(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

CREATE TABLE `users` (
  `id`         BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `email`      VARCHAR(50)  NOT NULL,
  `password`   VARCHAR(128) NOT NULL,
  `last_name`  VARCHAR(50)  NOT NULL,
  `first_name` VARCHAR(50)  NOT NULL,
  `patronymic` VARCHAR(50)  NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

CREATE TABLE `documents` (
  `id`            BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `document_name` VARCHAR(255) NOT NULL,
  `file_text`     LONGTEXT     NOT NULL,
  `from_user_id`  BIGINT(20)   NOT NULL,
  `to_user_id`    BIGINT(20)   NOT NULL,
  `send_date`     TIMESTAMP    NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `from_user_id_idx` (`from_user_id` ASC),
  CONSTRAINT `from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX `to_user_id_idx` (`to_user_id` ASC),
  CONSTRAINT `to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

CREATE TABLE `certificate_servers` (
  `id`  BIGINT(20)  NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`url`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;

CREATE TABLE `deferred_commands` (
  `id`           BIGINT(20)    NOT NULL AUTO_INCREMENT,
  `command_type` INT           NOT NULL,
  `query_string` VARCHAR(1000) NOT NULL,
  `timestamp`    BIGINT(20)    NOT NULL,
  `server_id`    BIGINT(20)    NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `server_id_idx` (`server_id` ASC),
  CONSTRAINT `server_id` FOREIGN KEY (`server_id`) REFERENCES `certificate_servers` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8;