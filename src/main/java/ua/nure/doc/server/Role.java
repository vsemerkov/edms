package ua.nure.doc.server;

/**
 * The <code>Role</code> class contains the enumeration of user roles.
 *
 * @author Volodymyr_Semerkov
 */
public enum Role {
    CLIENT, VISITOR;
}