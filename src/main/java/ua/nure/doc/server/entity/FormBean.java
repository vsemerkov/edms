package ua.nure.doc.server.entity;

/**
 * The <code>FormBean</code> class represents user registration data from input
 * form.
 *
 * @author Volodymyr_Semerkov
 */
public class FormBean {
    private String lastName;
    private String firstName;
    private String patronymic;
    private String email;
    private String password;
    private String passwordRepeat;
    private String dhPublicKey;
    private String curvePublicKey;

    public FormBean() {
    }

    public FormBean(String lastName, String firstName, String patronymic, String email,
                    String password, String passwordRepeat, String dhPublicKey, String curvePublicKey) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
        this.password = password;
        this.passwordRepeat = passwordRepeat;
        this.dhPublicKey = dhPublicKey;
        this.curvePublicKey = curvePublicKey;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public String getDHPublicKey() {
        return dhPublicKey;
    }

    public void setDHPublicKey(String dhPublicKey) {
        this.dhPublicKey = dhPublicKey;
    }

    public String getCurvePublicKey() {
        return curvePublicKey;
    }

    public void setCurvePublicKey(String curvePublicKey) {
        this.curvePublicKey = curvePublicKey;
    }
}
