package ua.nure.doc.server.entity;

import ua.nure.doc.server.certificate.CertificateType;

public class Certificate {
    private long id;
    private long userId;
    private CertificateType type;
    private String value;

    public Certificate() {
    }

    public Certificate(long userId, String value) {
        this.userId = userId;
        this.value = value;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public CertificateType getType() {
        return type;
    }

    public void setType(CertificateType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Certificate other = (Certificate) obj;
        return id == other.id;
    }

    @Override
    public int hashCode() {
        return new Long(id).hashCode() * 31;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Certificate [id=");
        sb.append(id);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", type=");
        sb.append(type.toString());
        sb.append("]");
        return sb.toString();
    }
}
