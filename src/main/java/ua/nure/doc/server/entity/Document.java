package ua.nure.doc.server.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * The <code>Document</code> class represents document data.
 *
 * @author Volodymyr_Semerkov
 */
public class Document implements Serializable, Cloneable {
    private long id;
    private long fromUserId;
    private long toUserId;
    private String documentName;
    private String documentValue;
    private Date sendDate;

    public Document() {
    }

    public Document(long id, long fromUserId, long toUserId) {
        this.id = id;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
    }

    public Document(String documentName, String documentValue, long fromUserId, long toUserId, Date sendDate) {
        this.documentName = documentName;
        this.documentValue = documentValue;
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.sendDate = sendDate;
    }

    public long getId() {
        return id;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentValue() {
        return documentValue;
    }

    public void setDocumentValue(String documentValue) {
        this.documentValue = documentValue;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Document [id=");
        sb.append(id);
        sb.append(", fromUserId=");
        sb.append(fromUserId);
        sb.append(", toUserId=");
        sb.append(toUserId);
        sb.append(", documentName=");
        sb.append(documentName);
        sb.append(", sendDate=");
        sb.append(sendDate);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        Document otherDocument = (Document) obj;
        return this.getId() == otherDocument.getId();
    }

    @Override
    public int hashCode() {
        return new Long(id).hashCode() * 31;
    }

    @Override
    public Document clone() throws CloneNotSupportedException {
        return (Document) super.clone();
    }
}
