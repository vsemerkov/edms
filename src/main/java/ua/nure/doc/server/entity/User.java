package ua.nure.doc.server.entity;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * The <code>User</code> class represents user registration data.
 *
 * @author Volodymyr_Semerkov
 */
public class User implements Serializable, Cloneable {
    private long id;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String email;
    private String password;

    public User() {
    }

    public User(long id, String email) {
        this.id = id;
        this.email = email;
    }

    public User(String lastName, String firstName, String patronymic, String email, String password) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("User [id=");
        sb.append(id);
        sb.append(", lastName=");
        sb.append(lastName);
        sb.append(", firstName=");
        sb.append(firstName);
        sb.append(", patronymic=");
        sb.append(patronymic);
        sb.append(", email=");
        sb.append(email);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        User otherUser = (User) obj;
        return this.getEmail().equals(otherUser.getEmail());
    }

    @Override
    public int hashCode() {
        return this.email.hashCode() * 31;
    }

    @Override
    public User clone() throws CloneNotSupportedException {
        return (User) super.clone();
    }
}
