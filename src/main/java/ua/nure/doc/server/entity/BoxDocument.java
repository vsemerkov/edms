package ua.nure.doc.server.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * The <codeBoxDocument</code> class represents document data.
 *
 * @author Volodymyr_Semerkov
 */
public class BoxDocument implements Serializable, Cloneable {
    private long id;
    private Participant fromParticipant;
    private Participant toParticipant;
    private String documentName;
    private Date sendDate;

    public BoxDocument() {
    }

    public BoxDocument(long id) {
        this.id = id;
    }

    public BoxDocument(Participant fromParticipant, Participant toParticipant, String documentName, Date sendDate) {
        this.fromParticipant = fromParticipant;
        this.toParticipant = toParticipant;
        this.documentName = documentName;
        this.sendDate = sendDate;
    }

    public long getId() {
        return id;
    }

    public Participant getFromParticipant() {
        return fromParticipant;
    }

    public void setFromParticipant(Participant fromParticipant) {
        this.fromParticipant = fromParticipant;
    }

    public Participant getToParticipant() {
        return toParticipant;
    }

    public void setToParticipant(Participant toParticipant) {
        this.toParticipant = toParticipant;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Recipient [id=");
        sb.append(id);
        sb.append(", fromParticipant=");
        sb.append(fromParticipant);
        sb.append(", toParticipant=");
        sb.append(toParticipant);
        sb.append(", documentName=");
        sb.append(documentName);
        sb.append(", sendDate=");
        sb.append(sendDate);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        BoxDocument otherBoxDocument = (BoxDocument) obj;
        return this.getId() == otherBoxDocument.getId();
    }

    @Override
    public int hashCode() {
        return new Long(id).hashCode() * 31;
    }

    @Override
    public BoxDocument clone() throws CloneNotSupportedException {
        return (BoxDocument) super.clone();
    }
}
