package ua.nure.doc.server.entity;

import java.io.Serializable;

/**
 * The <code>Recipient</code> class represents user data.
 *
 * @author Volodymyr_Semerkov
 */
public class Participant implements Serializable, Cloneable {
    private long id;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String email;

    public Participant() {
    }

    public Participant(long id, String email) {
        this.id = id;
        this.email = email;
    }

    public Participant(String lastName, String firstName, String patronymic, String email) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public String toParticipantString() {
        StringBuilder sb = new StringBuilder();
        sb.append(lastName);
        sb.append(" ");
        sb.append(firstName.charAt(0));
        sb.append(".");
        sb.append(patronymic.charAt(0));
        sb.append(". (");
        sb.append(email);
        sb.append(")");
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Recipient [id=");
        sb.append(id);
        sb.append(", lastName=");
        sb.append(lastName);
        sb.append(", firstName=");
        sb.append(firstName);
        sb.append(", patronymic=");
        sb.append(patronymic);
        sb.append(", email=");
        sb.append(email);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || (obj.getClass() != this.getClass()))
            return false;
        Participant otherParticipant = (Participant) obj;
        return this.getEmail().equals(otherParticipant.getEmail());
    }

    @Override
    public int hashCode() {
        return this.email.hashCode() * 31;
    }

    @Override
    public Participant clone() throws CloneNotSupportedException {
        return (Participant) super.clone();
    }
}
