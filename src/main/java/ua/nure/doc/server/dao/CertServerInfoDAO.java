package ua.nure.doc.server.dao;

import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.dao.exception.DAOException;

import java.util.List;

/**
 * Interface for access to certificate servers data.
 *
 * @author Volodymyr_Semrkov
 */
public interface CertServerInfoDAO {
    public boolean addCertServerInfo(CertServerInfo certServerInfo) throws DAOException;

    public boolean removeCertServerInfo(long id) throws DAOException;

    public boolean removeCertServerInfo(String url) throws DAOException;

    public CertServerInfo getCertServerInfo(String url) throws DAOException;

    public List<CertServerInfo> getCertServerInfoList() throws DAOException;

    public boolean containsCertServerInfo(String url) throws DAOException;
}
