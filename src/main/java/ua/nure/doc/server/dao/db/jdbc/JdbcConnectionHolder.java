package ua.nure.doc.server.dao.db.jdbc;

import java.sql.Connection;

/**
 * Holder for thread local connections.
 *
 * @author Volodymyr_Semrkov
 */
public class JdbcConnectionHolder {
    private static final ThreadLocal<Connection> LOCAL_CONNECTIONS = new ThreadLocal<Connection>();

    public static void setConnection(Connection connection) {
        LOCAL_CONNECTIONS.set(connection);
    }

    public static Connection getConnection() {
        return LOCAL_CONNECTIONS.get();
    }

    public static void removeConnection() {
        LOCAL_CONNECTIONS.remove();
    }
}
