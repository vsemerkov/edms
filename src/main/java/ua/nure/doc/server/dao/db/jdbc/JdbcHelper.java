package ua.nure.doc.server.dao.db.jdbc;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Connections provider.
 *
 * @author Volodymyr_Semrkov
 */
public class JdbcHelper {
    private static final Logger log = Logger.getLogger(JdbcHelper.class);
    private static DataSource dataSource;

    static {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            dataSource = (DataSource) envContext.lookup("jdbc/edms");
        } catch (NamingException ex) {
            log.error("Cannot obtain a connection from the pool!", ex);
        }
    }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
