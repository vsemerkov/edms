package ua.nure.doc.server.dao.mysql;

import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.dao.CertServerInfoDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcConnectionHolder;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.dao.mapper.CertServerInfoMapper;
import ua.nure.doc.server.db.DatabaseManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for access to certificate servers data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlCertServerInfoDAO implements CertServerInfoDAO {
    private static final String SQL__INSERT_CERT_SERVER = "INSERT INTO certificate_servers(url) VALUES(?)";
    private static final String SQL__REMOVE_CERT_SERVER_BY_ID = "DELETE FROM certificate_servers WHERE id=?";
    private static final String SQL__REMOVE_CERT_SERVER_BY_URL = "DELETE FROM certificate_servers WHERE url=?";
    private static final String SQL__GET_ALL_CERT_SERVERS = "SELECT * FROM certificate_servers";
    private static final String SQL__FIND_CERT_SERVER_BY_URL = "SELECT * FROM certificate_servers WHERE url=?";

    @Override
    public boolean addCertServerInfo(CertServerInfo certServerInfo) throws DAOException {
        if (containsCertServerInfo(certServerInfo.getURLPath())) {
            throw new DAOException(
                    "Certificate server information with such URL already exists in database");
        }
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_CERT_SERVER);
            CertServerInfoMapper.mapCertServerInfo(certServerInfo, pstmt);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeCertServerInfo(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_CERT_SERVER_BY_ID);
            pstmt.setLong(1, id);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeCertServerInfo(String url) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_CERT_SERVER_BY_URL);
            pstmt.setString(1, url);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<CertServerInfo> getCertServerInfoList() throws DAOException {
        List<CertServerInfo> certServerInfoList = new ArrayList<>();
        Statement stmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL__GET_ALL_CERT_SERVERS);
            while (rs.next()) {
                certServerInfoList.add(CertServerInfoMapper.unMapCertServerInfo(rs));
            }
            return certServerInfoList;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(stmt);
        }
    }

    @Override
    public CertServerInfo getCertServerInfo(String url) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_CERT_SERVER_BY_URL);
            pstmt.setString(1, url);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return CertServerInfoMapper.unMapCertServerInfo(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean containsCertServerInfo(String url) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_CERT_SERVER_BY_URL);
            pstmt.setString(1, url);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
