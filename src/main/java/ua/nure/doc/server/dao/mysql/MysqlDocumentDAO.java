package ua.nure.doc.server.dao.mysql;

import ua.nure.doc.server.dao.DocumentDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcConnectionHolder;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.dao.mapper.DocumentMapper;
import ua.nure.doc.server.db.DatabaseManager;
import ua.nure.doc.server.entity.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for access to document data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlDocumentDAO implements DocumentDAO {
    private static final String SQL__INSERT_DOCUMENT = "INSERT INTO documents(document_name, file_text, from_user_id, to_user_id, send_date) "
            + "VALUES(?, ?, ?, ?, ?)";
    private static final String SQL__FIND_DOCUMENT_BY_ID = "SELECT * FROM documents WHERE id=?";
    private static final String SQL__REMOVE_DOCUMENT_BY_ID = "DELETE FROM documents WHERE id=?";
    private static final String SQL__REMOVE_DOCUMENTS_BY_SENDER_ID = "DELETE FROM documents WHERE from_user_id=?";
    private static final String SQL__REMOVE_DOCUMENTS_BY_RECIPIENT_ID = "DELETE FROM documents WHERE to_user_id=?";
    private static final String SQL__GET_DOCUMENTS_BY_SENDER_ID = "SELECT * FROM documents WHERE from_user_id=? ORDER BY send_date DESC";
    private static final String SQL__GET_DOCUMENTS_BY_RECIPIENT_ID = "SELECT * FROM documents WHERE to_user_id=? ORDER BY send_date DESC";

    @Override
    public boolean addDocument(Document document) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_DOCUMENT);
            DocumentMapper.mapDocumentForAdd(document, pstmt);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public Document getDocument(long documentId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_DOCUMENT_BY_ID);
            pstmt.setLong(1, documentId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return DocumentMapper.unMapDocument(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeDocument(long documentId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_DOCUMENT_BY_ID);
            pstmt.setLong(1, documentId);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeDocumentsBySenderId(long senderId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_DOCUMENTS_BY_SENDER_ID);
            pstmt.setLong(1, senderId);
            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeDocumentsByRecipientId(long recipientId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_DOCUMENTS_BY_RECIPIENT_ID);
            pstmt.setLong(1, recipientId);
            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Document> getDocumentListBySenderId(long senderId) throws DAOException {
        List<Document> documentList = new ArrayList<>();
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__GET_DOCUMENTS_BY_SENDER_ID);
            pstmt.setLong(1, senderId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Document document = DocumentMapper.unMapDocument(rs);
                documentList.add(document);
            }
            return documentList;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Document> getDocumentListByRecipientId(long recipientId) throws DAOException {
        List<Document> documentList = new ArrayList<>();
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__GET_DOCUMENTS_BY_RECIPIENT_ID);
            pstmt.setLong(1, recipientId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Document document = DocumentMapper.unMapDocument(rs);
                documentList.add(document);
            }
            return documentList;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
