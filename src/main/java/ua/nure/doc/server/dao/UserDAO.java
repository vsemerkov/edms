package ua.nure.doc.server.dao;

import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.Participant;
import ua.nure.doc.server.entity.User;

import java.util.List;

/**
 * Interface for access to user data.
 *
 * @author Volodymyr_Semrkov
 */
public interface UserDAO {
    public boolean containsUser(String email) throws DAOException;

    public boolean addUser(User user) throws DAOException;

    public User getUser(String email) throws DAOException;

    public User getUser(long userId) throws DAOException;

    public boolean removeUser(String email) throws DAOException;

    public List<Participant> getRecipientList(long currentUserId) throws DAOException;

    public Participant getParticipant(long userId) throws DAOException;
}
