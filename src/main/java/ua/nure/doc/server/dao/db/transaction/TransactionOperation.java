package ua.nure.doc.server.dao.db.transaction;

import ua.nure.doc.server.dao.exception.DAOException;

/**
 * Main interface for the TransactionOperation pattern implementation.
 *
 * @author Volodymyr_Semerkov
 */
public interface TransactionOperation {
    public Object execute() throws DAOException;
}
