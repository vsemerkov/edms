package ua.nure.doc.server.dao.mapper;

import ua.nure.doc.server.entity.Document;
import ua.nure.doc.server.parameter.MapperParameters;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Document mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class DocumentMapper {
    public static Document unMapDocument(ResultSet rs) throws SQLException {
        Document document = new Document(rs.getLong(MapperParameters.ID),
                rs.getLong(MapperParameters.DOCUMENT__FROM_USER_ID), rs.getLong(MapperParameters.DOCUMENT__TO_USER_ID));
        document.setDocumentName(rs.getString(MapperParameters.DOCUMENT__NAME));
        document.setDocumentValue(rs.getString(MapperParameters.DOCUMENT__VALUE));
        document.setSendDate(rs.getTimestamp(MapperParameters.DOCUMENT__SEND_DATE));
        return document;
    }

    public static void mapDocumentForAdd(Document document, PreparedStatement pstmt)
            throws SQLException {
        pstmt.setString(1, document.getDocumentName());
        pstmt.setString(2, document.getDocumentValue());
        pstmt.setLong(3, document.getFromUserId());
        pstmt.setLong(4, document.getToUserId());
        pstmt.setTimestamp(5, new Timestamp(document.getSendDate().getTime()));
    }
}
