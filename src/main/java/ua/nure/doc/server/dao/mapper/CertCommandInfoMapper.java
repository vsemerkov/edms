package ua.nure.doc.server.dao.mapper;

import ua.nure.doc.server.certificate.CertParams;
import ua.nure.doc.server.certificate.command.CommandInfo;
import ua.nure.doc.server.certificate.command.CommandType;
import ua.nure.doc.server.parameter.MapperParameters;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Certificate server mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class CertCommandInfoMapper {

    public static CommandInfo unMapCertCommandInfo(ResultSet rs) throws SQLException {
        long id = rs.getLong(MapperParameters.ID);
        long timestamp = rs.getLong(MapperParameters.CERT_COMMAND__TIMESTAMP);
        CommandInfo commandInfo = new CommandInfo(id, timestamp);
        commandInfo.setURLPath(rs.getString(MapperParameters.CERT_SERVER__URL_PATH));
        commandInfo.setCommandType(CommandType.values()[rs.getInt(MapperParameters.CERT_COMMAND__COMMAND_TYPE)]);
        commandInfo.setQueryString(rs.getString(MapperParameters.CERT_COMMAND__QUERY_STRING));
        commandInfo.setRequestMethod(CertParams.REQUEST_METHOD_POST);
        return commandInfo;
    }

    public static void mapCertCommandInfo(CommandInfo commandInfo, long certServerId, PreparedStatement pstmt) throws SQLException {
        pstmt.setInt(1, commandInfo.getCommandType().ordinal());
        pstmt.setString(2, commandInfo.getQueryString());
        pstmt.setLong(3, commandInfo.getTimestamp());
        pstmt.setLong(4, certServerId);
    }
}
