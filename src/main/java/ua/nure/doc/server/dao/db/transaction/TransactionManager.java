package ua.nure.doc.server.dao.db.transaction;

import ua.nure.doc.server.dao.exception.DAOException;

/**
 * Main interface for the TransactionManager pattern implementation.
 *
 * @author Volodymyr_Semerkov
 */
public interface TransactionManager {
    public Object doInTransaction(TransactionOperation operation)
            throws DAOException, DAOException;
}
