package ua.nure.doc.server.dao;

import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.User;

import java.math.BigInteger;
import java.util.Map;

/**
 * Interface for access to public parameters data.
 *
 * @author Volodymyr_Semrkov
 */
public interface ParamDAO {
    public Map<String, BigInteger> getParams() throws DAOException;

    public boolean addParams(BigInteger p, BigInteger g) throws DAOException;

    public boolean containsParams() throws DAOException;
}
