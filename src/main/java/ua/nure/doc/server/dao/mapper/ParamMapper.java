package ua.nure.doc.server.dao.mapper;

import ua.nure.doc.server.AppParameters;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.MapperParameters;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Parameters mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class ParamMapper {

    public static Map<String, BigInteger> unMapParam(ResultSet rs) throws SQLException {
        Map<String, BigInteger> paramMap = new HashMap<>();
        paramMap.put(AppParameters.P, rs.getBigDecimal(AppParameters.P).toBigInteger());
        paramMap.put(AppParameters.G, rs.getBigDecimal(AppParameters.G).toBigInteger());
        return paramMap;
    }
}
