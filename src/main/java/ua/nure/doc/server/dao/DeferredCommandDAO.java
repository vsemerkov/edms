package ua.nure.doc.server.dao;

import ua.nure.doc.server.certificate.command.CommandInfo;
import ua.nure.doc.server.dao.exception.DAOException;

import java.util.Set;

/**
 * Interface for access to certificate server`s commands data.
 *
 * @author Volodymyr_Semrkov
 */
public interface DeferredCommandDAO {
    public boolean addCertCommandInfo(CommandInfo commandInfo, long certServerId) throws DAOException;

    public boolean removeCertCommandInfo(long id) throws DAOException;

    public Set<CommandInfo> getCertCommandInfoSet(long certServerId) throws DAOException;

    public boolean containsCommandsForServer(long certServerId) throws DAOException;
}
