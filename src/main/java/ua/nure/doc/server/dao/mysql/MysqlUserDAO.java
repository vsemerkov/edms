package ua.nure.doc.server.dao.mysql;

import ua.nure.doc.server.dao.UserDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcConnectionHolder;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.dao.mapper.UserMapper;
import ua.nure.doc.server.db.DatabaseManager;
import ua.nure.doc.server.entity.Participant;
import ua.nure.doc.server.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for access to user data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlUserDAO implements UserDAO {
    private static final String SQL__FIND_USER_BY_EMAIL = "SELECT * FROM users WHERE email=?";
    private static final String SQL__FIND_USER_BY_ID = "SELECT * FROM users WHERE id=?";
    private static final String SQL__INSERT_USER = "INSERT INTO users(email, password, last_name, first_name, patronymic) "
            + "VALUES(?, ?, ?, ?, ?)";
    private static final String SQL__REMOVE_USER = "DELETE FROM users WHERE email=?";
    private static final String SQL__GET_RECIPIENTS = "SELECT id, email, last_name, first_name, patronymic FROM users WHERE id <> ?";
    private static final String SQL__GET_PARTICIPANT = "SELECT id, email, last_name, first_name, patronymic FROM users WHERE id = ?";

    @Override
    public boolean containsUser(String email) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_USER_BY_EMAIL);
            pstmt.setString(1, email);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean addUser(User user) throws DAOException {
        if (containsUser(user.getEmail())) {
            throw new DAOException(
                    "User with such email address already exists in database!");
        }
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_USER);
            UserMapper.mapUserForAdd(user, pstmt);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public User getUser(String email) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_USER_BY_EMAIL);
            pstmt.setString(1, email);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return UserMapper.unMapUser(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public User getUser(long userId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_USER_BY_ID);
            pstmt.setLong(1, userId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return UserMapper.unMapUser(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeUser(String email) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_USER);
            pstmt.setString(1, email);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public List<Participant> getRecipientList(long currentUserId) throws DAOException {
        List<Participant> recipientList = new ArrayList<>();
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__GET_RECIPIENTS);
            pstmt.setLong(1, currentUserId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Participant recipient = UserMapper.unMapParticipant(rs);
                recipientList.add(recipient);
            }
            return recipientList;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public Participant getParticipant(long userId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__GET_PARTICIPANT);
            pstmt.setLong(1, userId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return UserMapper.unMapParticipant(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
