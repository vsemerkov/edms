package ua.nure.doc.server.dao.mapper;

import ua.nure.doc.server.entity.Participant;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.MapperParameters;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class UserMapper {
    public static User unMapUser(ResultSet rs) throws SQLException {
        User user = new User(rs.getLong(MapperParameters.ID),
                rs.getString(MapperParameters.USER__EMAIL));
        user.setPassword(rs.getString(MapperParameters.USER__PASSWORD));
        user.setLastName(rs.getString(MapperParameters.USER__LAST_NAME));
        user.setFirstName(rs.getString(MapperParameters.USER__FIRST_NAME));
        user.setPatronymic(rs.getString(MapperParameters.USER__PATRONYMIC));
        return user;
    }

    public static Participant unMapParticipant(ResultSet rs) throws SQLException {
        Participant participant = new Participant(rs.getLong(MapperParameters.ID),
                rs.getString(MapperParameters.USER__EMAIL));
        participant.setLastName(rs.getString(MapperParameters.USER__LAST_NAME));
        participant.setFirstName(rs.getString(MapperParameters.USER__FIRST_NAME));
        participant.setPatronymic(rs.getString(MapperParameters.USER__PATRONYMIC));
        return participant;
    }

    public static void mapUserForAdd(User user, PreparedStatement pstmt)
            throws SQLException {
        pstmt.setString(1, user.getEmail());
        pstmt.setString(2, user.getPassword());
        pstmt.setString(3, user.getLastName());
        pstmt.setString(4, user.getFirstName());
        pstmt.setString(5, user.getPatronymic());
    }
}
