package ua.nure.doc.server.dao.mysql;

import ua.nure.doc.server.certificate.command.CommandInfo;
import ua.nure.doc.server.dao.DeferredCommandDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcConnectionHolder;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.dao.mapper.CertCommandInfoMapper;
import ua.nure.doc.server.db.DatabaseManager;

import java.sql.*;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class for access to certificate server`s commands data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlDeferredCommandDAO implements DeferredCommandDAO {
    private static final String SQL__INSERT_CERT_COMMAND = "INSERT INTO deferred_commands(command_type, query_string, timestamp, server_id) " +
            "VALUES(?, ?, ?, ?)";
    private static final String SQL__REMOVE_CERT_COMMAND_BY_ID = "DELETE FROM deferred_commands WHERE id=?";
    private static final String SQL__FIND_CERT_COMMAND_BY_SERVER_ID = "SELECT deferred_commands.*, certificate_servers.url FROM deferred_commands, certificate_servers WHERE server_id=?";

    @Override
    public boolean addCertCommandInfo(CommandInfo commandInfo, long certServerId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_CERT_COMMAND);
            CertCommandInfoMapper.mapCertCommandInfo(commandInfo, certServerId, pstmt);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean removeCertCommandInfo(long id) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__REMOVE_CERT_COMMAND_BY_ID);
            pstmt.setLong(1, id);
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public Set<CommandInfo> getCertCommandInfoSet(long certServerId) throws DAOException {
        Set<CommandInfo> commandInfoSet = new TreeSet<>();
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_CERT_COMMAND_BY_SERVER_ID);
            pstmt.setLong(1, certServerId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                commandInfoSet.add(CertCommandInfoMapper.unMapCertCommandInfo(rs));
            }
            return commandInfoSet;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean containsCommandsForServer(long certServerId) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__FIND_CERT_COMMAND_BY_SERVER_ID);
            pstmt.setLong(1, certServerId);
            return pstmt.executeQuery().first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }
}
