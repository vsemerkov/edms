package ua.nure.doc.server.dao.mapper;

import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.parameter.MapperParameters;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Certificate server mapper.
 *
 * @author Volodymyr_Semrkov
 */
public class CertServerInfoMapper {

    public static CertServerInfo unMapCertServerInfo(ResultSet rs) throws SQLException {
        CertServerInfo certServerInfo = new CertServerInfo(rs.getLong(MapperParameters.ID));
        certServerInfo.setURLPath(rs.getString(MapperParameters.CERT_SERVER__URL_PATH));
        return certServerInfo;
    }

    public static void mapCertServerInfo(CertServerInfo certServerInfo, PreparedStatement pstmt) throws SQLException {
        pstmt.setString(1, certServerInfo.getURLPath());
    }
}
