package ua.nure.doc.server.dao.mysql;

import ua.nure.doc.server.dao.ParamDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcConnectionHolder;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.dao.mapper.ParamMapper;
import ua.nure.doc.server.db.DatabaseManager;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.util.Map;

/**
 * Class for access to public parameters data.
 *
 * @author Volodymyr_Semrkov
 */
public class MysqlParamDAO implements ParamDAO {
    private static final String SQL__GET_PARAMS = "SELECT * FROM params";
    private static final String SQL__INSERT_PARAMS = "INSERT INTO params(p, g) VALUES(?, ?)";

    @Override
    public Map<String, BigInteger> getParams() throws DAOException {
        Statement stmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL__GET_PARAMS);
            if (rs.next()) {
                return ParamMapper.unMapParam(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(stmt);
        }
    }

    @Override
    public boolean addParams(BigInteger p, BigInteger g) throws DAOException {
        PreparedStatement pstmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            pstmt = connection.prepareStatement(SQL__INSERT_PARAMS);
            pstmt.setBigDecimal(1, new BigDecimal(p));
            pstmt.setBigDecimal(2, new BigDecimal(g));
            return pstmt.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(pstmt);
        }
    }

    @Override
    public boolean containsParams() throws DAOException {
        Statement stmt = null;
        try {
            Connection connection = JdbcConnectionHolder.getConnection();
            stmt = connection.createStatement();
            return stmt.executeQuery(SQL__GET_PARAMS).first();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseManager.closeStatement(stmt);
        }
    }
}
