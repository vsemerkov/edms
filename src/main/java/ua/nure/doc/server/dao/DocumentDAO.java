package ua.nure.doc.server.dao;

import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.Document;

import java.util.List;

/**
 * Interface for access to document data.
 *
 * @author Volodymyr_Semrkov
 */
public interface DocumentDAO {
    public boolean addDocument(Document document) throws DAOException;

    public Document getDocument(long documentId) throws DAOException;

    public boolean removeDocument(long documentId) throws DAOException;

    public boolean removeDocumentsBySenderId(long senderId) throws DAOException;

    public boolean removeDocumentsByRecipientId(long recipientId) throws DAOException;

    public List<Document> getDocumentListBySenderId(long senderId) throws DAOException;

    public List<Document> getDocumentListByRecipientId(long recipientId) throws DAOException;
}
