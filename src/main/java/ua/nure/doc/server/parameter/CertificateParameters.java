package ua.nure.doc.server.parameter;

/**
 * Holder for certificate parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public interface CertificateParameters {
    public static final String CERTIFICATE__ID = "id";
    public static final String CERTIFICATE__USER_ID = "userId";
    public static final String CERTIFICATE__VALUE = "value";
    public static final String CERTIFICATE__TYPE = "type";
}
