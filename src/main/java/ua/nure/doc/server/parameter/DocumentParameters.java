package ua.nure.doc.server.parameter;

/**
 * Holder for document parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public interface DocumentParameters {
    public static final String DOCUMENT = "document";
    public static final String DOCUMENT_NAME = "documentName";
    public static final String DOCUMENT__ID = "documentId";
    public static final String DOCUMENT__RECIPIENT_ID = "recipientId";
}
