package ua.nure.doc.server.parameter;

/**
 * Holder for mapper parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public interface MapperParameters {

    // Total mapper parameter.
    public static final String ID = "id";

    // User mapper parameters.
    public static final String USER__LAST_NAME = "last_name";
    public static final String USER__FIRST_NAME = "first_name";
    public static final String USER__PATRONYMIC = "patronymic";
    public static final String USER__EMAIL = "email";
    public static final String USER__PASSWORD = "password";
    public static final String USER__PUBLIC_KEY = "public_key";

    //Certificate server mapper parameter
    public static final String CERT_SERVER__URL_PATH = "url";

    //Certificate command mapper parameters
    public static final String CERT_COMMAND__COMMAND_TYPE = "command_type";
    public static final String CERT_COMMAND__QUERY_STRING = "query_string";
    public static final String CERT_COMMAND__TIMESTAMP = "timestamp";
    public static final String CERT_COMMAND__SERVER_ID = "server_id";

    // Document mapper parameters.
    public static final String DOCUMENT__NAME = "document_name";
    public static final String DOCUMENT__VALUE = "file_text";
    public static final String DOCUMENT__FROM_USER_ID = "from_user_id";
    public static final String DOCUMENT__TO_USER_ID = "to_user_id";
    public static final String DOCUMENT__SEND_DATE = "send_date";
}
