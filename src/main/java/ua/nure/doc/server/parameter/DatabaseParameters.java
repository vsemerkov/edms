package ua.nure.doc.server.parameter;

/**
 * Holder for database parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public interface DatabaseParameters {
    public static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
    public static final String DB_SERVER_URL = "jdbc:mysql://localhost:3306";
    public static final String DB_NAME = "edms";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/edms";
    public static final String USER = "root";
    public static final String PASSWORD = "root";
}
