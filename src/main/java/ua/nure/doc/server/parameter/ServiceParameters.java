package ua.nure.doc.server.parameter;

/**
 * Holder for service parameters names.
 *
 * @author Volodymyr_Semerkov
 */
public interface ServiceParameters {
    //services
    public static final String DOCUMENT_SERVICE = "documentService";
    public static final String USER_SERVICE = "userService";
    public static final String PARAM_SERVICE = "paramService";
    public static final String CERTIFICATE_SERVICE = "certificateService";
    public static final String CERTIFICATE_SERVER_INFO_SERVICE = "certServerInfoService";
    public static final String DEFERRED_COMMAND_SERVICE = "deferredCommandService";

    // security parameters
    public static final String SECURITY_XML = "securityXML";

    //certificate servers parameters
    public static final String CERTIFICATE_SERVERS_XML = "certificateServersXML";

    // captcha parameters
    public static final String CAPTCHA__MODE = "captchaMode";
    public static final String CAPTCHA__MODE_SESSION = "session";
    public static final String CAPTCHA__MODE_COOKIES = "cookies";
    public static final String CAPTCHA__MODE_HIDDEN = "hidden";
    public static final String CAPTCHA__MANAGER = "captchaManager";
    public static final String CAPTCHA__ID = "captchaId";
    public static final String CAPTCHA__CODE = "captchaCode";
    public static final String CAPTCHA__TIME_OUT = "captchaTimeout";

    // captcha thread parameters
    public static final String CHECK_TIME = "checkPeriod";

    //revocation server parameters
    public static final String REVOCATION_SERVER = "revocationServer";
}
