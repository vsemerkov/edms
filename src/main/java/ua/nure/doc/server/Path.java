package ua.nure.doc.server;

/**
 * Path holder.
 *
 * @author Volodymyr_Semerkov
 */
public interface Path {
    // pages
    public static final String PAGE__INDEX = "/WEB-INF/pages/internal/index.jsp";
    public static final String PAGE__OUTBOX = "/WEB-INF/pages/internal/outbox.jsp";
    public static final String PAGE__SEND_FILE = "/WEB-INF/pages/internal/sendDocument.jsp";
    public static final String PAGE__CHANGE_KEYS = "/WEB-INF/pages/internal/changeKeys.jsp";

    public static final String PAGE__SIGN_UP = "/WEB-INF/pages/external/signup.jsp";
    public static final String PAGE__SIGN_IN = "/WEB-INF/pages/external/signin.jsp";
    public static final String PAGE__LOGOUT = "logout.jsp";
    public static final String PAGE__ERROR_PAGE = "error_page.jsp";
    public static final String PAGE__ACCESS_ERROR = "/WEB-INF/pages/internal/access_error_page.jsp";

    // commands
    public static final String COMMAND__SIGN_UP = "registration";
    public static final String COMMAND__INDEX = "index";
}
