package ua.nure.doc.server.listener;

import org.apache.log4j.Logger;
import ua.nure.doc.server.AppParameters;
import ua.nure.doc.server.certificate.CertServerStAXParser;
import ua.nure.doc.server.dao.*;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.dao.mysql.*;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.security.KeyGenerator;
import ua.nure.doc.server.security.SimpleKeyGenerator;
import ua.nure.doc.server.security.captcha.manager.CaptchaManager;
import ua.nure.doc.server.security.captcha.manager.CookiesCaptchaManager;
import ua.nure.doc.server.security.captcha.manager.HiddenCaptchaManager;
import ua.nure.doc.server.security.captcha.manager.SessionCaptchaManager;
import ua.nure.doc.server.service.*;
import ua.nure.doc.server.service.thread.DeferredCommandManager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.stream.XMLStreamException;
import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Context listener.
 *
 * @author Volodymyr_Semerkov
 */
public class ContextListener implements ServletContextListener {
    private ScheduledExecutorService scheduler;
    private static final Logger log = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        if (log.isDebugEnabled()) {
            log.debug("Servlet context initialization starts");
        }
        ServletContext servletContext = event.getServletContext();
        setServletContextAttributes(servletContext);
        startDeferredCommandManager(servletContext);
        if (log.isDebugEnabled()) {
            log.debug("Servlet context initialization finished");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (log.isDebugEnabled()) {
            log.debug("Servlet context destruction starts");
        }
        scheduler.shutdownNow();
        if (log.isDebugEnabled()) {
            log.debug("Servlet context destruction finished");
        }
    }

    private void setServletContextAttributes(ServletContext servletContext) {
        setUserServiceAttribute(servletContext);
        setDocumentServiceAttribute(servletContext);
        setParamServiceAttribute(servletContext);
        setCaptchaManagerAttribute(servletContext);
        setCertServerInfoServiceAttribute(servletContext);
        setDeferredCommandServiceAttribute(servletContext);
        setCertificateServiceAttribute(servletContext);
    }

    private void setUserServiceAttribute(ServletContext servletContext) {
        UserDAO userDAO = new MysqlUserDAO();
        UserService userService = new UserService(userDAO);
        servletContext.setAttribute(ServiceParameters.USER_SERVICE,
                userService);
        log.debug("UserDAO was created");
    }

    private void setDocumentServiceAttribute(ServletContext servletContext) {
        DocumentDAO documentDAO = new MysqlDocumentDAO();
        DocumentService documentService = new DocumentService(documentDAO);
        servletContext.setAttribute(ServiceParameters.DOCUMENT_SERVICE,
                documentService);
        log.debug("DocumentDAO was created");
    }

    private void setParamServiceAttribute(ServletContext servletContext) {
        ParamDAO paramDAO = new MysqlParamDAO();
        ParamService paramService = new ParamService(paramDAO);
        try {
            if (!paramService.contains()) {
                Map<String, BigInteger> paramMap = SimpleKeyGenerator.generateKeys();
                paramService.add(paramMap.get(AppParameters.P), paramMap.get(AppParameters.G));
            }
        } catch (DAOException e) {
            log.error(e.getMessage());
        }
        servletContext.setAttribute(ServiceParameters.PARAM_SERVICE,
                paramService);
        log.debug("ParamDAO was created");
    }

    private void setCaptchaManagerAttribute(ServletContext servletContext) {
        String captchaMode = servletContext
                .getInitParameter(ServiceParameters.CAPTCHA__MODE);
        log.trace("captchaMode --> " + captchaMode);
        int captchaTimeout = Integer.parseInt(servletContext
                .getInitParameter(ServiceParameters.CAPTCHA__TIME_OUT));
        log.trace("captchaTimeout --> " + captchaTimeout + " seconds.");
        int checkPeriod = Integer.parseInt(servletContext
                .getInitParameter(ServiceParameters.CHECK_TIME));
        log.trace("checkPeriod --> " + checkPeriod + " seconds.");
        CaptchaManager captchaManager = null;
        switch (captchaMode) {
            case ServiceParameters.CAPTCHA__MODE_SESSION:
                captchaManager = new SessionCaptchaManager(captchaTimeout);
                break;
            case ServiceParameters.CAPTCHA__MODE_COOKIES:
                captchaManager = new CookiesCaptchaManager(captchaTimeout,
                        checkPeriod);
                break;
            case ServiceParameters.CAPTCHA__MODE_HIDDEN:
                captchaManager = new HiddenCaptchaManager(captchaTimeout,
                        checkPeriod);
                break;
        }
        if (captchaManager == null || captchaTimeout <= 0 || checkPeriod <= 0) {
            log.error("Captcha mode or timeout is incorrect");
            throw new IllegalStateException(
                    "Captcha mode or timeout is incorrect");
        }
        servletContext.setAttribute(ServiceParameters.CAPTCHA__MANAGER,
                captchaManager);
    }

    private void setCertServerInfoServiceAttribute(ServletContext servletContext) {
        String xmlFileName = servletContext
                .getInitParameter(ServiceParameters.CERTIFICATE_SERVERS_XML);
        boolean isValidate;
        try {
            isValidate = CertServerStAXParser.validate(xmlFileName);
        } catch (XMLStreamException e) {
            log.error("Certificate XML file validation error");
            throw new IllegalStateException("Certificate XML file validation error");
        }
        if (isValidate) {
            try {
                CertServerInfoDAO certServerInfoDAO = new MysqlCertServerInfoDAO();
                log.debug("CertServerInfoDAO was created");
                CertServerInfoService certServerInfoService = new CertServerInfoService(certServerInfoDAO, CertServerStAXParser.parse(xmlFileName));
                servletContext.setAttribute(ServiceParameters.CERTIFICATE_SERVER_INFO_SERVICE,
                        certServerInfoService);
                log.debug("Certificate servers info XML file was read");
            } catch (XMLStreamException e) {
                log.error("Certificate XML file parsing error");
                throw new IllegalStateException("Certificate XML file parsing error");
            }
        } else {
            log.error("Certificate XML file is not valid");
            throw new IllegalStateException("Certificate XML file is not valid");
        }
    }

    private void setDeferredCommandServiceAttribute(ServletContext servletContext) {
        DeferredCommandDAO deferredCommandDAO = new MysqlDeferredCommandDAO();
        log.debug("DeferredCommandDAO was created");
        DeferredCommandService deferredCommandService = new DeferredCommandService(deferredCommandDAO);
        servletContext.setAttribute(ServiceParameters.DEFERRED_COMMAND_SERVICE, deferredCommandService);
    }

    private void setCertificateServiceAttribute(ServletContext servletContext) {
        DeferredCommandService deferredCommandService = (DeferredCommandService) servletContext.getAttribute(ServiceParameters.DEFERRED_COMMAND_SERVICE);
        CertificateService certificateService = new CertificateService(deferredCommandService);
        servletContext.setAttribute(ServiceParameters.CERTIFICATE_SERVICE,
                certificateService);
    }

    private void startDeferredCommandManager(ServletContext servletContext) {
        CertServerInfoService certServerInfoService = (CertServerInfoService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVER_INFO_SERVICE);
        DeferredCommandService deferredCommandService = (DeferredCommandService) servletContext.getAttribute(ServiceParameters.DEFERRED_COMMAND_SERVICE);
        scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new DeferredCommandManager(certServerInfoService, deferredCommandService), 0, 15, TimeUnit.MINUTES);
    }
}
