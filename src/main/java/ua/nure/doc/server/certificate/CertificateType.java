package ua.nure.doc.server.certificate;

/**
 * Certificate type enum
 *
 * @author Volodymyr_Semerkov
 */
public enum CertificateType {
    DH, CURVE
}
