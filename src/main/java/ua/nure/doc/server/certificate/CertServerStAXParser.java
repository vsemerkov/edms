package ua.nure.doc.server.certificate;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.doc.server.certificate.entity.CertServerInfo;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for certificate servers StAX parser.
 *
 * @author Volodymyr_Semerkov
 */
public class CertServerStAXParser extends DefaultHandler {
    private static final String SERVERS = "servers";
    private static final String SERVER = "server";
    private static final String XSD_FILE_PATH = "/servers.xsd";

    /**
     * Validate XML document.
     *
     * @param xmlFileName XML document name.
     * @return
     * @throws javax.xml.stream.XMLStreamException
     */
    public static boolean validate(String xmlFileName)
            throws XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        try {
            reader = inputFactory.createXMLStreamReader(CertServerStAXParser.class
                    .getResourceAsStream(xmlFileName));
            SchemaFactory schemaFactory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(CertServerStAXParser.class
                    .getResource(XSD_FILE_PATH));
            Validator validator = schema.newValidator();
            validator.validate(new StAXSource(reader));
        } catch (XMLStreamException | SAXException | IOException e) {
            return false;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return true;
    }

    /**
     * Parse XML document with StAX.
     *
     * @param xmlFileName XML document name.
     * @return Collection of constraints.
     * @throws javax.xml.stream.XMLStreamException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public static List<CertServerInfo> parse(String xmlFileName)
            throws XMLStreamException {
        List<CertServerInfo> certServerInfoList = null;
        CertServerInfo currentCertServerInfo = null;
        String currentElement = null;
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = inputFactory
                .createXMLStreamReader(CertServerStAXParser.class
                        .getResourceAsStream(xmlFileName));
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    currentElement = reader.getLocalName();
                    if (SERVERS.equals(currentElement)) {
                        certServerInfoList = new ArrayList<CertServerInfo>();
                    }
                    if (SERVER.equals(currentElement)) {
                        currentCertServerInfo = new CertServerInfo();
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    String text = reader.getText().trim();
                    if (text.isEmpty())
                        break;
                    if (SERVER.equals(currentElement)) {
                        currentCertServerInfo.setURLPath(text);
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    currentElement = reader.getLocalName();
                    if (SERVER.equals(currentElement)) {
                        certServerInfoList.add(currentCertServerInfo);
                    }
                    currentElement = null;
                    break;
            }
        }
        reader.close();
        return certServerInfoList;
    }
}
