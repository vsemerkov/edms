package ua.nure.doc.server.certificate;

import java.util.Map;
import java.util.Objects;

/**
 * Query string builder
 *
 * @author Volodymyr_Semerkov
 */
public final class QueryStringBuilder {
    /**
     * @param paramMap Parameter map.
     * @return Query string if the parameter map is not{@code null} otherwise {@code NullPointerException}.
     * @throws NullPointerException if {@code paramMap} is {@code null}
     */
    public static String makeQueryString(Map<String, String> paramMap) {
        Objects.requireNonNull(paramMap, "Parameter map must not be null");
        StringBuilder sb = new StringBuilder();
        if (paramMap.size() > 0) {
            paramMap.forEach((key, value) -> {
                sb.append(key);
                sb.append("=");
                sb.append(value);
                sb.append("&");
            });
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }
}
