package ua.nure.doc.server.certificate;

/**
 * Certificate constants.
 *
 * @author Volodymyr_Semerkov
 */
public interface CertParams {
    //parameters
    public static final String PARAMETER__USER_ID = "userId";
    public static final String PARAMETER__CERT_ID = "certId";
    public static final String PARAMETER__CERT_TYPE = "certType";
    public static final String PARAMETER__CERT = "cert";
    public static final String PARAMETER__RESULT = "result";
    public static final String PARAMETER__REVOKE_DATE = "revokeDate";

    //request methods
    public static final String REQUEST_METHOD_GET = "GET";
    public static final String REQUEST_METHOD_POST = "POST";
}
