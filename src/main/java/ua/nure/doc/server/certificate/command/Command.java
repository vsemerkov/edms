package ua.nure.doc.server.certificate.command;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import ua.nure.doc.server.certificate.CertParams;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Thread for execution of request to certificate server.
 *
 * @author Volodymyr_Semerkov
 */
public class Command implements Runnable {
    private CommandInfo commandInfo;

    private CommandResult commandResult;

    public Command(CommandInfo commandInfo) {
        this.commandInfo = commandInfo;
    }

    public CommandInfo getCommandInfo() {
        return commandInfo;
    }

    public CommandResult getCommandResult() {
        return commandResult;
    }

    @Override
    public void run() {
        switch (commandInfo.getRequestMethod()) {
            case CertParams.REQUEST_METHOD_GET:
                executeGet();
                break;
            case CertParams.REQUEST_METHOD_POST:
                executePost();
                break;
            default:
                throw new IllegalArgumentException("Incorrect request method");
        }
    }

    private void executeGet() {
        boolean hasErrors = false;
        boolean success = false;
        Object result = null;
        HttpURLConnection con = null;
        InputStream in = null;
        try {
            URL url = new URL(commandInfo.getURLPathWithCommand() + "?" + commandInfo.getQueryString());
            con = (HttpURLConnection) url.openConnection();
            if (con.getResponseCode() == 200) {
                success = true;
                in = con.getInputStream();
                JSONObject jsonObject = new JSONObject(IOUtils.toString(in, "UTF-8"));
                result = jsonObject.get(CertParams.PARAMETER__RESULT);
            }
        } catch (Exception e) {
            hasErrors = true;
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    hasErrors = true;
                }
            }
            if (con != null) {
                con.disconnect();
            }
        }
        commandResult = new CommandResult(hasErrors, success, result);
    }

    private void executePost() {
        boolean hasErrors = false;
        boolean success = false;
        Object result = null;
        HttpURLConnection con = null;
        DataOutputStream wr = null;
        try {
            URL url = new URL(commandInfo.getURLPathWithCommand());
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(commandInfo.getRequestMethod());
            con.setDoOutput(true);
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(commandInfo.getQueryString());
            wr.flush();
            if (con.getResponseCode() == 200) {
                success = true;
            }
        } catch (Exception e) {
            hasErrors = true;
        } finally {
            if (wr != null) {
                try {
                    wr.close();
                } catch (IOException e) {
                    hasErrors = true;
                }
            }
            if (con != null) {
                con.disconnect();
            }
        }
        commandResult = new CommandResult(hasErrors, success, result);
    }
}
