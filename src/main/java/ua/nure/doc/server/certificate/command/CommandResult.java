package ua.nure.doc.server.certificate.command;

/**
 * Command execution result.
 *
 * @author Volodymyr_Semerkov
 */
public class CommandResult {
    private boolean hasErrors;
    private boolean success;
    private Object result;

    public CommandResult(boolean hasErrors, boolean success, Object result) {
        this.hasErrors = hasErrors;
        this.success = success;
        this.result = result;
    }

    public boolean hasErrors() {
        return hasErrors;
    }

    public boolean isSuccess() {
        return success;
    }

    public Object getResult() {
        return result;
    }
}
