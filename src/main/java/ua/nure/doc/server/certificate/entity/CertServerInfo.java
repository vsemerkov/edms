package ua.nure.doc.server.certificate.entity;

/**
 * The <code>CertificateServerInfo</code> class contains information about certificate server.
 *
 * @author Volodymyr_Semerkov
 */
public class CertServerInfo {
    private long id;
    private String URLPath;

    public CertServerInfo() {
    }

    public CertServerInfo(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getURLPath() {
        return URLPath;
    }

    public void setURLPath(String URLPath) {
        this.URLPath = URLPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CertServerInfo that = (CertServerInfo) o;

        if (URLPath != null ? !URLPath.equals(that.URLPath) : that.URLPath != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (URLPath != null ? URLPath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CertificateServerInfo [URLPath=");
        sb.append(URLPath);
        sb.append("]");
        return sb.toString();
    }
}
