package ua.nure.doc.server.certificate.command;

/**
 * Command information.
 *
 * @author Volodymyr_Semerkov
 */
public class CommandInfo implements Comparable<CommandInfo> {
    private long id;
    private String URLPath;
    private CommandType commandType;
    private String requestMethod;
    private String queryString;
    private long timestamp;

    public CommandInfo(String URLPath, CommandType commandType, String requestMethod, String queryString, long timestamp) {
        this.URLPath = URLPath;
        this.commandType = commandType;
        this.requestMethod = requestMethod;
        this.queryString = queryString;
        this.timestamp = timestamp;
    }

    public CommandInfo(long id, long timestamp) {
        this.id = id;
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getURLPath() {
        return URLPath;
    }

    public void setURLPath(String URLPath) {
        this.URLPath = URLPath;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getURLPathWithCommand() {
        return URLPath + "/" + commandType.toString().toLowerCase();
    }

    @Override
    public int compareTo(CommandInfo anotherCommandInfo) {
        return Long.compare(this.timestamp, anotherCommandInfo.timestamp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommandInfo that = (CommandInfo) o;

        if (id != that.id) return false;
        if (timestamp != that.timestamp) return false;
        if (!URLPath.equals(that.URLPath)) return false;
        if (commandType != that.commandType) return false;
        if (!queryString.equals(that.queryString)) return false;
        if (!requestMethod.equals(that.requestMethod)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + URLPath.hashCode();
        result = 31 * result + commandType.hashCode();
        result = 31 * result + requestMethod.hashCode();
        result = 31 * result + queryString.hashCode();
        return result;
    }
}
