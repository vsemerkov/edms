package ua.nure.doc.server.certificate.command;

/**
 * Certificate server command type.
 */
public enum CommandType {
    ADD("add"), GET("get"), GET_REVOKED("getrevoked"), REMOVE("remove"), REVOKE("revoke"), UNREVOKE("unrevoke");

    private final String name;

    private CommandType(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
