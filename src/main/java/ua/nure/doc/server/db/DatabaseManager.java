package ua.nure.doc.server.db;

import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.parameter.DatabaseParameters;

import java.sql.*;
import java.util.Scanner;

public class DatabaseManager {

    public static boolean create(String queryFilePath) throws DAOException {
        Connection connection = null;
        Statement stmt = null;
        Scanner scanner = null;
        try {
            connection = getConnection();
            stmt = connection.createStatement();
            scanner = new Scanner(
                    DatabaseManager.class.getResourceAsStream(queryFilePath));
            scanner.useDelimiter(";");
            while (scanner.hasNext()) {
                String str = scanner.next();
                stmt.execute(str);
            }
            connection.commit();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DAOException(e);
        } finally {
            scanner.close();
            closeStatementAndConnection(stmt, connection);
        }
    }

    public static boolean drop(String databaseName) throws DAOException {
        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = getConnection();
            pstmt = connection.prepareStatement(String.format(
                    "DROP DATABASE %s", databaseName));
            pstmt.executeUpdate();
            connection.commit();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            throw new DAOException(e);
        } finally {
            closeStatementAndConnection(pstmt, connection);
        }
    }

    private static Connection getConnection() throws ClassNotFoundException,
            SQLException {
        Class.forName(DatabaseParameters.DB_DRIVER_NAME);
        Connection connection = DriverManager.getConnection(
                DatabaseParameters.DB_SERVER_URL, DatabaseParameters.USER,
                DatabaseParameters.PASSWORD);
        connection.setAutoCommit(false);
        return connection;
    }

    public static void closeStatement(Statement stmt) throws DAOException {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    private static void closeStatementAndConnection(Statement stmt,
                                                    Connection connection) throws DAOException {
        closeStatement(stmt);
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
