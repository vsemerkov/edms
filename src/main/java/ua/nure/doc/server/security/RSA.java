package ua.nure.doc.server.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class RSA {
    public static long[] primeArray = getArrayOfPrimeNumbers(201);

    public static Map<String, Long> generateKeys() {
        Random rnd = new Random();
        int rndNumber = rnd.nextInt(rnd.nextInt(100));
        long p = primeArray[rndNumber + 100];
        rndNumber = rnd.nextInt(rnd.nextInt(100));
        long q = primeArray[rndNumber + 100];
        while (p == q) {
            rndNumber = rnd.nextInt(rnd.nextInt(100));
            q = primeArray[rndNumber + 100];
        }
        long n = p * q;
        long e = 0;
        long f = (p - 1) * (q - 1);
        for (int i = 6; i < primeArray.length; i++) {
            if (f % primeArray[i] != 0) {
                e = primeArray[i];
                break;
            }
        }
        long d = 1;
        do {
            d += 2;
        } while ((d * e) % f != 1);
        Map<String, Long> keys = new HashMap<String, Long>();
        keys.put("e", e);
        keys.put("d", d);
        keys.put("n", n);
        System.out.println("Server p = " + p);
        System.out.println("Server q = " + q);
        System.out.println("Server e = " + e);
        System.out.println("Server d = " + d);
        System.out.println("Server n = " + n);
        return keys;
    }

    public static long code(long e, long n, long m) {
        return pow(m, e, n);
    }

    public static long pow(long base, long exp, long mod) {
        long i = 31;
        for (; i >= 0; i--) {
            if ((1L & (exp >>> i)) == 1) break;
        }
        if (i < 0) return 1;
        long res = base % mod;
        for (long j = i - 1; j >= 0; j--) {
            res = res * res % mod;
            if ((1L & (exp >>> j)) == 1) res = (res * base) % mod;
        }
        return res;
    }

    public static boolean isPrime(long ch) {
        for (int i = 2; i <= Math.sqrt(ch); i++) {
            if (ch % i == 0)
                return false;
        }
        return true;
    }

    private static long[] getArrayOfPrimeNumbers(int size) {
        long[] primeArray = new long[size];
        int count = 0;
        long number = 1;
        while (count != size) {
            number += 2;
            if (isPrime(number)) {
                primeArray[count] = number;
                count++;
            }
        }
        return primeArray;
    }
}
