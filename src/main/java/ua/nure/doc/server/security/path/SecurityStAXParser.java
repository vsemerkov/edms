package ua.nure.doc.server.security.path;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.doc.server.Role;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for security StAX parser.
 *
 * @author Volodymyr_Semerkov
 */
public class SecurityStAXParser extends DefaultHandler {
    private static final String SECURITY = "security";
    private static final String CONSTRAINT = "constraint";
    private static final String URL_PATTERN = "url-pattern";
    private static final String ROLE = "role";
    private static final String XSD_FILE_PATH = "/security.xsd";

    /**
     * Validate XML document.
     *
     * @param xmlFileName XML document name.
     * @return
     * @throws javax.xml.stream.XMLStreamException
     */
    public static boolean validate(String xmlFileName)
            throws XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        try {
            reader = inputFactory.createXMLStreamReader(SecurityStAXParser.class
                    .getResourceAsStream(xmlFileName));
            SchemaFactory schemaFactory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(SecurityStAXParser.class
                    .getResource(XSD_FILE_PATH));
            Validator validator = schema.newValidator();
            validator.validate(new StAXSource(reader));
        } catch (XMLStreamException | SAXException | IOException e) {
            return false;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return true;
    }

    /**
     * Parse XML document with StAX.
     *
     * @param xmlFileName XML document name.
     * @return Collection of constraints.
     * @throws javax.xml.stream.XMLStreamException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public static List<Constraint> parse(String xmlFileName)
            throws XMLStreamException {
        List<Constraint> constraints = null;
        Constraint currentConstraint = null;
        String currentElement = null;
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = inputFactory
                .createXMLStreamReader(SecurityStAXParser.class
                        .getResourceAsStream(xmlFileName));
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    currentElement = reader.getLocalName();
                    if (SECURITY.equals(currentElement)) {
                        constraints = new ArrayList<Constraint>();
                    }
                    if (CONSTRAINT.equals(currentElement)) {
                        currentConstraint = new Constraint();
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    String text = reader.getText().trim();
                    if (text.isEmpty())
                        break;
                    if (URL_PATTERN.equals(currentElement)) {
                        currentConstraint.setURLPattern(text);
                    }
                    if (ROLE.equals(currentElement)) {
                        Role role = Role.valueOf(text.toUpperCase());
                        currentConstraint.getRoles().add(role);
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    currentElement = reader.getLocalName();
                    if (CONSTRAINT.equals(currentElement)) {
                        constraints.add(currentConstraint);
                    }
                    currentElement = null;
                    break;
            }
        }
        reader.close();
        return constraints;
    }
}
