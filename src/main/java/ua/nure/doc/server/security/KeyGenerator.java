package ua.nure.doc.server.security;

import ua.nure.doc.server.AppParameters;

import java.math.BigInteger;
import java.util.*;

public class KeyGenerator {
    private static final int N = 300;

    private static BigInteger[] primeArray = generateArrayOfPrimeNumbers(N);

    private static BigInteger[] generateArrayOfPrimeNumbers(int size) {
        Set<BigInteger> primeNumberSet = new HashSet<>();
        Random random = new Random();
        while (primeNumberSet.size() != size) {
            BigInteger primeNumber = BigInteger.probablePrime(128, random);
            primeNumberSet.add(primeNumber);
        }
        return primeNumberSet.stream().sorted().toArray(length -> new BigInteger[length]);
    }

    public static Map<String, BigInteger> generateKeys() {
        Random rnd = new Random();
        int rndNumber = rnd.nextInt(rnd.nextInt(100));
        BigInteger p = primeArray[rndNumber];
        BigInteger t = (p.subtract(BigInteger.ONE)).divide(BigInteger.valueOf(2));
        while (!t.isProbablePrime(1)) {
            rndNumber++;
            p = primeArray[rndNumber];
            t = (p.subtract(BigInteger.ONE)).divide(BigInteger.valueOf(2));
        }
        BigInteger fp = p.subtract(BigInteger.ONE);
        rndNumber = rnd.nextInt(rnd.nextInt(100));
        BigInteger g = primeArray[rndNumber];
        while (!g.modPow(fp, p).equals(BigInteger.ONE)) {
            rndNumber++;
            g = primeArray[rndNumber];
        }
        Map<String, BigInteger> keys = new HashMap<String, BigInteger>();
        keys.put(AppParameters.P, p);
        keys.put(AppParameters.G, g);
        return keys;
    }

    public static BigInteger getRandomPrimeNumber() {
        Random random = new Random();
        int rndNumber = random.nextInt(random.nextInt(N));
        return primeArray[rndNumber];
    }
}
