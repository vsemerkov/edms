package ua.nure.doc.server.security;

/**
 * The <code>LoginStatus</code> class contains the enumeration of login
 * statuses.
 *
 * @author Volodymyr_Semerkov
 */
public enum LoginStatus {
    CORRECT, INCORRECT;
}
