package ua.nure.doc.server.security.captcha.container;

import org.apache.log4j.Logger;
import ua.nure.doc.server.security.captcha.entity.Captcha;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Class <code>CaptchaContainer</code> contains map which stores captcha id as
 * key and captcha object as value. Captcha object has next fields: captcha
 * creation time in long format and code on captcha.
 *
 * @author Volodymyr_Semerkov
 */
public class CaptchaContainer {
    private ConcurrentMap<Long, Captcha> captchaMap;
    private static final Logger log = Logger.getLogger(CaptchaContainer.class);

    public CaptchaContainer() {
        this.captchaMap = new ConcurrentHashMap<Long, Captcha>();
    }

    /**
     * Add captcha id and code of captcha in container.
     *
     * @param id
     * @param captchaCode
     */
    public synchronized void addCaptcha(long id, String captchaCode) {
        Long creationTime = Calendar.getInstance().getTimeInMillis();
        Captcha captcha = new Captcha(captchaCode, creationTime);
        captchaMap.put(id, captcha);
    }

    public synchronized boolean containsCaptcha(long captchaId,
                                                String userCaptchaCode) {
        Captcha captcha = captchaMap.get(captchaId);
        if (captcha == null)
            return false;
        String captchaCode = captcha.getCode();
        if (log.isTraceEnabled()) {
            log.trace("captchaCode --> " + captchaCode);
            log.trace("userCaptchaCode --> " + userCaptchaCode);
        }
        return userCaptchaCode.equals(captchaCode);
    }

    public Captcha getCaptchaById(long id) {
        return captchaMap.get(id);
    }

    public Captcha removeCaptchaById(long id) {
        return captchaMap.remove(id);
    }

    public synchronized void removeOldElements(long timeout) {
        Iterator<Entry<Long, Captcha>> captchaIterator = captchaMap.entrySet()
                .iterator();
        while (captchaIterator.hasNext()) {
            Entry<Long, Captcha> captcha = captchaIterator.next();
            if (captcha.getValue().getCreationTime() + timeout <= Calendar
                    .getInstance().getTimeInMillis()) {
                log.debug("Captcha " + captcha.getKey() + " was removed");
                captchaIterator.remove();
            }
        }
    }
}
