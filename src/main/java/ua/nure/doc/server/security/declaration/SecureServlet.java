package ua.nure.doc.server.security.declaration;

import org.apache.log4j.Logger;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.Role;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.security.declaration.annotation.Allow;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Servlet implementation class SecureServlet
 */
public abstract class SecureServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(SecureServlet.class);

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Class<?> runtimeClass = this.getClass();
        Allow classAnnotation = runtimeClass.getAnnotation(Allow.class);
        List<Role> classUserRoles = new ArrayList<Role>();
        if (classAnnotation != null) {
            classUserRoles = Arrays.asList(classAnnotation.value());
        }
        if (log.isTraceEnabled()) {
            log.trace("Class user roles " + classUserRoles);
        }
        String requestMethodName = request.getMethod();
        if (log.isTraceEnabled()) {
            log.trace("Request method name " + requestMethodName);
        }
        String servletMethodName = ServletMethodNameContainer.getServletMethodName(requestMethodName);
        if (log.isTraceEnabled()) {
            log.trace("Servlet method name " + servletMethodName);
        }
        try {
            Method method = runtimeClass.getDeclaredMethod(servletMethodName, HttpServletRequest.class, HttpServletResponse.class);
            Allow methodAnnotation = method.getAnnotation(Allow.class);
            List<Role> methodUserRoles = new ArrayList<Role>();
            if (methodAnnotation != null) {
                methodUserRoles = Arrays.asList(methodAnnotation.value());
            }
            if (log.isTraceEnabled()) {
                log.trace("Method user roles " + methodUserRoles);
            }
            User user = (User) request.getSession().getAttribute(
                    UserParameters.USER);
            Role role = user != null ? Role.CLIENT : Role.VISITOR;
            if (classUserRoles.contains(role) || methodUserRoles.contains(role)) {
                method.invoke(this, request, response);
            } else {
                if (role == Role.VISITOR) {
                    RequestDispatcher requestDispatcher = request
                            .getRequestDispatcher(Path.PAGE__SIGN_IN);
                    requestDispatcher.forward(request, response);
                } else {
                    response.sendError(403);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
            return;
        }
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doDelete(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doOptions(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doPut(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doTrace(HttpServletRequest request,
                        HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doHead(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
    }
}
