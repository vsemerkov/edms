package ua.nure.doc.server.security.captcha;

import org.apache.log4j.Logger;
import ua.nure.doc.server.security.captcha.container.CaptchaContainer;

/**
 * Remove captcha object from map when period of time is running out.
 *
 * @author Volodymyr_Semerkov
 */
public class CaptchaTimeoutCheck implements Runnable {
    private long checkPeriod;
    private long captchaTimeout;
    private CaptchaContainer captchaContainer;
    private static final Logger log = Logger
            .getLogger(CaptchaTimeoutCheck.class);

    public CaptchaTimeoutCheck(int checkPeriod, int captchaTimeout,
                               CaptchaContainer captchaContainer) {
        this.checkPeriod = checkPeriod * 1000;
        this.captchaTimeout = captchaTimeout * 1000;
        this.captchaContainer = captchaContainer;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(checkPeriod);
                captchaContainer.removeOldElements(captchaTimeout);
            } catch (InterruptedException e) {
                log.error("InterruptedException");
                return;
            }
        }
    }
}
