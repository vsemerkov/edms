package ua.nure.doc.server.security.declaration.annotation;

import ua.nure.doc.server.Role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Allow {
    Role[] value() default {Role.VISITOR};
}