package ua.nure.doc.server.security;

import ua.nure.doc.server.AppParameters;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SimpleKeyGenerator {
    public static long[] primeArray = getArrayOfPrimeNumbers(300);

    public static Map<String, BigInteger> generateKeys() {
        Random rnd = new Random();
        int rndNumber = rnd.nextInt(rnd.nextInt(100));
        long p = primeArray[rndNumber];
        long t = (p - 1) / 2;
        while (!RSA.isPrime(t)) {
            rndNumber++;
            p = primeArray[rndNumber];
            t = (p - 1) / 2;
        }
        long fp = p - 1;
        rndNumber = rnd.nextInt(rnd.nextInt(100));
        long g = primeArray[rndNumber];
        while (RSA.pow(g, fp, p) != 1) {
            rndNumber++;
            g = primeArray[rndNumber];
        }
        Map<String, BigInteger> keys = new HashMap<>();
        keys.put(AppParameters.P, BigInteger.valueOf(p));
        keys.put(AppParameters.G, BigInteger.valueOf(g));
        return keys;
    }

    public static long generatePrimeNumber() {
        Random rnd = new Random();
        int rndNumber = rnd.nextInt(rnd.nextInt(300));
        return primeArray[rndNumber];
    }

    private static long[] getArrayOfPrimeNumbers(int size) {
        long[] primeArray = new long[size];
        int count = 0;
        long number = 65537;
        while (count != size) {
            number += 2;
            if (RSA.isPrime(number)) {
                primeArray[count] = number;
                count++;
            }
        }
        return primeArray;
    }
}
