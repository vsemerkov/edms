package ua.nure.doc.server.security.captcha;

import ua.nure.doc.server.security.captcha.entity.GeneratedCaptcha;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * This class contains methods which generate captcha.
 *
 * @author Volodymyr_Semerkov
 */
public class CaptchaGenerator {
    private static final int WIDTH = 200;
    private static final int HEIGHT = 100;
    private static final String SOURCE = "0123456789";
    private static Random random = new Random();

    public static GeneratedCaptcha generateCaptcha() {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT,
                BufferedImage.TYPE_INT_RGB);
        Graphics gr = image.getGraphics();
        int fontSize = 60;
        Font font = new Font("SansSerif", Font.PLAIN, fontSize);
        gr.setFont(font);
        String code = generateCode();
        gr.setColor(Color.GRAY);
        gr.fillRect(0, 0, WIDTH, HEIGHT);
        gr.setColor(Color.LIGHT_GRAY);
        gr.drawString(code, 21, HEIGHT - fontSize + 20);
        gr.setColor(Color.GRAY);
        gr.drawString(code, 23, HEIGHT - fontSize + 12);
        gr.setColor(Color.LIGHT_GRAY);
        gr.drawString(code, 25, HEIGHT - fontSize + 23);
        for (int i = 0; i < 45; i++) {
            gr.setColor(new Color(random.nextInt(256), random.nextInt(256),
                    random.nextInt(256)));
            gr.drawLine(random.nextInt(WIDTH), random.nextInt(HEIGHT),
                    random.nextInt(WIDTH), random.nextInt(HEIGHT));
            for (int j = 1; j < 10; j++) {
                gr.drawOval(random.nextInt(WIDTH), random.nextInt(HEIGHT), 1, 1);
            }
        }
        image.flush();
        return new GeneratedCaptcha(code, image);
    }

    private static String generateCode() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            sb.append(SOURCE.charAt(random.nextInt(SOURCE.length())));
        }
        return sb.toString();
    }
}
