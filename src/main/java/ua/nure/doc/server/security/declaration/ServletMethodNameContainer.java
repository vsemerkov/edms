package ua.nure.doc.server.security.declaration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents store of servlet method names.
 *
 * @author Volodymyr_Semerkov
 */
public class ServletMethodNameContainer {
    private static Map<String, String> methodNameMap = new HashMap<String, String>();

    static {
        methodNameMap.put("GET", "doGet");
        methodNameMap.put("POST", "doPost");
        methodNameMap.put("DELETE", "doDelete");
        methodNameMap.put("OPTIONS", "doOptions");
        methodNameMap.put("PUT", "doPut");
        methodNameMap.put("TRACE", "doTrace");
        methodNameMap.put("HEAD", "doHead");

    }

    public static Map<String, String> getMethodNameMap() {
        return Collections.unmodifiableMap(methodNameMap);
    }

    public static String getServletMethodName(String requestMethodName) {
        if (requestMethodName == null || !methodNameMap.containsKey(requestMethodName)) {
            return null;
        }
        return methodNameMap.get(requestMethodName);
    }
}
