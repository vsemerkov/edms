package ua.nure.doc.server.security.captcha.manager;

import org.apache.log4j.Logger;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.security.captcha.CaptchaTimeoutCheck;
import ua.nure.doc.server.security.captcha.container.CaptchaContainer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicLong;

public class CookiesCaptchaManager implements CaptchaManager {
    private AtomicLong counter = new AtomicLong();
    private int timeout;
    private CaptchaContainer captchaContainer = new CaptchaContainer();
    private Thread captchaTimeoutCheck;
    private static final Logger log = Logger
            .getLogger(CookiesCaptchaManager.class);

    public CookiesCaptchaManager(int timeout, int checkPeriod) {
        this.timeout = timeout;
        captchaTimeoutCheck = new Thread(new CaptchaTimeoutCheck(checkPeriod,
                timeout, captchaContainer));
        captchaTimeoutCheck.start();
    }

    @Override
    public void setCaptchaCode(HttpServletRequest request,
                               HttpServletResponse response, String code) {
        try {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(ServiceParameters.CAPTCHA__ID)) {
                    long id = Long.parseLong(cookie.getValue());
                    if (log.isTraceEnabled()) {
                        log.trace("id --> " + id);
                    }
                    captchaContainer.addCaptcha(id, code);
                    return;
                }
            }
        } catch (NullPointerException | NumberFormatException e) {
            throw new IllegalStateException("Captcha id is null!");
        }
    }

    @Override
    public boolean validateCaptcha(HttpServletRequest request) {
        String userCaptchaCode = (String) request
                .getParameter(UserParameters.USER__CAPTCHA);
        Cookie[] cookies = request.getCookies();
        if (userCaptchaCode == null || cookies == null)
            return false;
        long id = -1;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(ServiceParameters.CAPTCHA__ID)) {
                id = Long.parseLong(cookie.getValue());
                if (log.isTraceEnabled()) {
                    log.trace("id --> " + id);
                }
                break;
            }
        }
        return captchaContainer.containsCaptcha(id, userCaptchaCode);
    }

    @Override
    public void setId(HttpServletRequest request, HttpServletResponse response) {
        long id = counter.getAndIncrement();
        Cookie cookie = new Cookie(ServiceParameters.CAPTCHA__ID,
                String.valueOf(id));
        cookie.setMaxAge(timeout);
        response.addCookie(cookie);
    }
}
