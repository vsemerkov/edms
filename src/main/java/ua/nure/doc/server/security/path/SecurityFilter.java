package ua.nure.doc.server.security.path;

import org.apache.log4j.Logger;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.Role;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

/**
 * Security filter.
 *
 * @author Volodymyr_Semerkov
 */
public class SecurityFilter implements Filter {
    private SecurityManager securityManager;
    private static final Logger log = Logger.getLogger(SecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        if (log.isDebugEnabled()) {
            log.debug("Filter initialization starts");
        }
        ServletContext servletContext = filterConfig.getServletContext();
        String xmlFileName = servletContext
                .getInitParameter(ServiceParameters.SECURITY_XML);
        boolean isValidate;
        try {
            isValidate = SecurityStAXParser.validate(xmlFileName);
        } catch (XMLStreamException e) {
            log.error("Security XML file validation error");
            throw new IllegalStateException("Security XML file validation error");
        }
        if (isValidate) {
            try {
                securityManager = new SecurityManager(
                        SecurityStAXParser.parse(xmlFileName));
            } catch (XMLStreamException e) {
                log.error("Security XML file parsing error");
                throw new IllegalStateException("Security XML file parsing error");
            }
        } else {
            log.error("Security XML file is not valid");
            throw new IllegalStateException("Security XML file is not valid");
        }
        if (log.isDebugEnabled()) {
            log.debug("Filter initialization finished");
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        User user = (User) httpRequest.getSession().getAttribute(
                UserParameters.USER);
        Role role = user != null ? Role.CLIENT : Role.VISITOR;
        String pagePath = httpRequest.getServletPath();
        if (securityManager.accept(pagePath, role)) {
            chain.doFilter(httpRequest, httpResponse);
            return;
        } else {
            if (role == Role.VISITOR) {
                RequestDispatcher requestDispatcher = httpRequest
                        .getRequestDispatcher(Path.PAGE__SIGN_IN);
                requestDispatcher.forward(httpRequest, httpResponse);
                return;
            } else {
                httpResponse.sendError(403);
                return;
            }
        }
    }

    @Override
    public void destroy() {
    }
}
