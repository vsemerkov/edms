package ua.nure.doc.server.security.captcha.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CaptchaManager {
	public abstract void setId(HttpServletRequest request,
							   HttpServletResponse response);

	public abstract void setCaptchaCode(HttpServletRequest request,
										HttpServletResponse response, String code);

	public abstract boolean validateCaptcha(HttpServletRequest request);
}
