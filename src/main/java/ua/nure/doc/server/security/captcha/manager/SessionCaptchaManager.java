package ua.nure.doc.server.security.captcha.manager;

import org.apache.log4j.Logger;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionCaptchaManager implements CaptchaManager {
    private int timeout;
    private static final Logger log = Logger
            .getLogger(SessionCaptchaManager.class);

    public SessionCaptchaManager(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public void setCaptchaCode(HttpServletRequest request,
                               HttpServletResponse response, String code) {
        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(timeout);
        if (log.isTraceEnabled()) {
            log.trace("code --> " + code);
        }
        session.setAttribute(ServiceParameters.CAPTCHA__CODE, code);
    }

    @Override
    public boolean validateCaptcha(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String userCaptchaCode = (String) request
                .getParameter(UserParameters.USER__CAPTCHA);
        if (log.isTraceEnabled()) {
            log.trace("userCaptchaCode --> " + userCaptchaCode);
        }
        String captchaCode = (String) session
                .getAttribute(ServiceParameters.CAPTCHA__CODE);
        if (log.isTraceEnabled()) {
            log.trace("captchaCode --> " + captchaCode);
        }
        if (userCaptchaCode == null || captchaCode == null)
            return false;
        return userCaptchaCode.equals(captchaCode);
    }

    @Override
    public void setId(HttpServletRequest request, HttpServletResponse response) {
    }
}
