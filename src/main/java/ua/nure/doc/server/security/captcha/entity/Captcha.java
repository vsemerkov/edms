package ua.nure.doc.server.security.captcha.entity;

public class Captcha {
    private String code;
    private long creationTime;

    public Captcha() {
    }

    public Captcha(String code, long creationTime) {
        this.code = code;
        this.creationTime = creationTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get creation time in milliseconds.
     *
     * @return Creation time in milliseconds.
     */
    public long getCreationTime() {
        return creationTime;
    }

    /**
     * Set creation time in milliseconds.
     *
     * @param creationTime Creation time in milliseconds.
     */
    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }
}
