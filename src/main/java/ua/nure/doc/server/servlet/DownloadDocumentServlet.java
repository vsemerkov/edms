package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.entity.Document;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.DocumentParameters;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.service.DocumentService;
import ua.nure.doc.server.service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class DownloadDocumentServlet extends HttpServlet {
    private DocumentService documentService;
    private UserService userService;
    private static final Logger log = Logger.getLogger(DownloadDocumentServlet.class);

    public DownloadDocumentServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        documentService = (DocumentService) servletContext.getAttribute(ServiceParameters.DOCUMENT_SERVICE);
        userService = (UserService) servletContext.getAttribute(ServiceParameters.USER_SERVICE);
        if (documentService == null) {
            log.error("Document service attribute is not exists.");
            throw new IllegalStateException(
                    "Document service attribute is not exists.");
        }
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method started");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            long documentId = Long.valueOf(request.getParameter(DocumentParameters.DOCUMENT__ID));
            Document document = documentService.getDocument(documentId);
            if (document != null) {
                if (document.getToUserId() == user.getId()) {
                    JSONObject resultJson = new JSONObject();
                    resultJson.put(DocumentParameters.DOCUMENT_NAME, document.getDocumentName());
                    resultJson.put(DocumentParameters.DOCUMENT, document.getDocumentValue());
                    response.setContentType("text/json");
                    PrintWriter writer = response.getWriter();
                    writer.write(resultJson.toString());
                    writer.flush();
                    if (log.isDebugEnabled()) {
                        log.debug("JSON was sent");
                    }
                } else {
                    throw new IllegalStateException("Document has other recipient!");
                }
            } else {
                throw new IllegalStateException("Document is not exists!");
            }
        } catch (Exception e) {
            log.error("Server error", e);
            response.setStatus(400);
        }
    }
}
