package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.certificate.CertParams;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.service.CertServerInfoService;
import ua.nure.doc.server.service.CertificateService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


public class RevokeUserCertificateServlet extends HttpServlet {
    private CertificateService certificateService;
    private CertServerInfoService certServerInfoService;
    private String revocationServerPath;
    private static final Logger log = Logger.getLogger(RevokeUserCertificateServlet.class);

    public RevokeUserCertificateServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        certificateService = (CertificateService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVICE);
        if (certificateService == null) {
            log.error("Certificate service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate service attribute is not exists.");
        }
        certServerInfoService = (CertServerInfoService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVER_INFO_SERVICE);
        if (certServerInfoService == null) {
            log.error("Certificate server info service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate server info service attribute is not exists.");
        }
        revocationServerPath = servletContext.getInitParameter(ServiceParameters.REVOCATION_SERVER);
        if (revocationServerPath == null) {
            log.error("Revocation server path init parameter is not exists.");
            throw new IllegalStateException(
                    "Certificate server info service attribute is not exists.");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method started");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        List<CertServerInfo> certServerInfoList = null;
        long certId = 0;
        try {
            certId = Long.valueOf(request.getParameter(CertParams.PARAMETER__CERT_ID));
            certServerInfoList = certServerInfoService.getCertServerInfoList();
            boolean result = certificateService.revoke(certServerInfoList, revocationServerPath, user.getId(), certId);
            if (!result) {
                certificateService.unrevoke(certServerInfoList, certId);
            }
            JSONObject resultJson = new JSONObject();
            resultJson.put(CertParams.PARAMETER__RESULT, result);
            response.setContentType("text/json");
            PrintWriter writer = response.getWriter();
            writer.write(resultJson.toString());
            writer.flush();
            if (log.isDebugEnabled()) {
                log.debug("JSON was sent");
            }
        } catch (Exception e) {
            try {
                certificateService.unrevoke(certServerInfoList, certId);
            } catch (Exception e1) {
                log.error("Server error", e1);
            }
            log.error("Server error", e);
            response.setStatus(400);
        }
    }
}
