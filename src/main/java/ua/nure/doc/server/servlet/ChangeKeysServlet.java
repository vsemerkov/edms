package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.certificate.CertParams;
import ua.nure.doc.server.certificate.CertificateType;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.entity.Certificate;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.service.CertServerInfoService;
import ua.nure.doc.server.service.CertificateService;
import ua.nure.doc.server.service.DocumentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


public class ChangeKeysServlet extends HttpServlet {
    private CertificateService certificateService;
    private CertServerInfoService certServerInfoService;
    private DocumentService documentService;
    private String revocationServerPath;
    private static final Logger log = Logger.getLogger(ChangeKeysServlet.class);

    public ChangeKeysServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        certificateService = (CertificateService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVICE);
        if (certificateService == null) {
            log.error("Certificate service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate service attribute is not exists.");
        }
        certServerInfoService = (CertServerInfoService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVER_INFO_SERVICE);
        if (certServerInfoService == null) {
            log.error("Certificate server info service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate server info service attribute is not exists.");
        }
        documentService = (DocumentService) servletContext.getAttribute(ServiceParameters.DOCUMENT_SERVICE);
        if (documentService == null) {
            log.error("Document service attribute is not exists.");
            throw new IllegalStateException(
                    "Document service attribute is not exists.");
        }
        revocationServerPath = servletContext.getInitParameter(ServiceParameters.REVOCATION_SERVER);
        if (revocationServerPath == null) {
            log.error("Revocation server path init parameter is not exists.");
            throw new IllegalStateException(
                    "Certificate server info service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method started");
        }
        RequestDispatcher dispatcher = request
                .getRequestDispatcher(Path.PAGE__CHANGE_KEYS);
        dispatcher.forward(request, response);
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method started");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            String dhPublicKey = request.getParameter(UserParameters.USER__DH_PUBLIC_KEY);
            String curvePublicKey = request.getParameter(UserParameters.USER__CURVE_PUBLIC_KEY);
            if (dhPublicKey != null && !dhPublicKey.isEmpty() && curvePublicKey != null && !curvePublicKey.isEmpty()) {
                List<CertServerInfo> certServerInfoList = certServerInfoService.getCertServerInfoList();
                Certificate dhCertificate = certificateService.getCertificate(certServerInfoList, user.getId(), CertificateType.DH);
                Certificate curveCertificate = certificateService.getCertificate(certServerInfoList, user.getId(), CertificateType.CURVE);
                boolean result = true;
                if (dhCertificate != null) {
                    result = result && certificateService.revoke(certServerInfoList, revocationServerPath, user.getId(), dhCertificate.getId());
                }
                if (curveCertificate != null) {
                    result = result && certificateService.revoke(certServerInfoList, revocationServerPath, user.getId(), curveCertificate.getId());
                }
                if (result) {
                    result = result && certificateService.add(certServerInfoList, user.getId(), CertificateType.DH, dhPublicKey);
                    result = result && certificateService.add(certServerInfoList, user.getId(), CertificateType.CURVE, curvePublicKey);
                    documentService.removeDocumentsBySenderId(user.getId());
                    documentService.removeDocumentsByRecipientId(user.getId());
                } else {
                    certificateService.unrevoke(certServerInfoList, dhCertificate.getId());
                    certificateService.unrevoke(certServerInfoList, curveCertificate.getId());
                }
                JSONObject resultJson = new JSONObject();
                resultJson.put(CertParams.PARAMETER__RESULT, result);
                response.setContentType("text/json");
                PrintWriter writer = response.getWriter();
                writer.write(resultJson.toString());
                writer.flush();
                if (log.isDebugEnabled()) {
                    log.debug("JSON was sent");
                }
            } else {
                throw new IllegalArgumentException("Incorrect request parameters");
            }
        } catch (Exception e) {
            log.error("Server error", e);
            response.setStatus(400);
        }
    }
}
