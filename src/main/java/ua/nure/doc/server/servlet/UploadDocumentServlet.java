package ua.nure.doc.server.servlet;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.entity.Document;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.DocumentParameters;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.service.DocumentService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ua.nure.doc.server.service.UserService;


public class UploadDocumentServlet extends HttpServlet {
    private DocumentService documentService;
    private UserService userService;
    private static final Logger log = Logger.getLogger(UploadDocumentServlet.class);

    public UploadDocumentServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        documentService = (DocumentService) servletContext.getAttribute(ServiceParameters.DOCUMENT_SERVICE);
        userService = (UserService) servletContext.getAttribute(ServiceParameters.USER_SERVICE);
        if (documentService == null) {
            log.error("Document service attribute is not exists.");
            throw new IllegalStateException(
                    "Document service attribute is not exists.");
        }
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method started");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            Map<String, String> paramMap = getParamMap(request);
            String documentName = paramMap.get(DocumentParameters.DOCUMENT_NAME);
            documentName = documentName.substring(documentName.lastIndexOf("\\") + 1);
            long fromUserId = user.getId();
            long toUserId = Long.valueOf(paramMap.get(DocumentParameters.DOCUMENT__RECIPIENT_ID));
            Date sendDate = new Date();
            Document document = new Document(documentName, paramMap.get(DocumentParameters.DOCUMENT), fromUserId, toUserId, sendDate);
            documentService.addDocument(document);
            JSONObject resultJson = new JSONObject();
            resultJson.put("result", true);
            response.setContentType("text/json");
            PrintWriter writer = response.getWriter();
            writer.write(resultJson.toString());
            writer.flush();
            if (log.isDebugEnabled()) {
                log.debug("JSON was sent");
            }
        } catch (Exception e) {
            log.error("Server error", e);
            response.setStatus(400);
        }
    }

    private Map<String, String> getParamMap(HttpServletRequest request) throws IOException {
        InputStream inputStream = request.getInputStream();
        StringBuilder sb = new StringBuilder(IOUtils.toString(inputStream, "UTF-8"));
        sb = new StringBuilder(URLDecoder.decode(sb.toString(), "UTF-8"));
        Map<String, String> paramMap = new HashMap<>();
        sb.append("&");
        int ampPos = sb.indexOf("&");
        while (ampPos != -1) {
            int eqPos = sb.indexOf("=");
            String paramName = sb.substring(0, eqPos);
            String value = sb.substring(eqPos + 1, ampPos);
            sb.delete(0, ampPos + 1);
            paramMap.put(paramName, value);
            ampPos = sb.indexOf("&");
        }
        return paramMap;
    }
}
