package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.BoxDocument;
import ua.nure.doc.server.entity.Document;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.service.DocumentService;
import ua.nure.doc.server.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class <code>IndexServlet</code>
 */
public class IndexServlet extends HttpServlet {
    private DocumentService documentService;
    private UserService userService;
    private static final Logger log = Logger.getLogger(IndexServlet.class);

    public IndexServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        documentService = (DocumentService) servletContext.getAttribute(ServiceParameters.DOCUMENT_SERVICE);
        if (documentService == null) {
            log.error("Document service attribute is not exists.");
            throw new IllegalStateException(
                    "Document service attribute is not exists.");
        }
        userService = (UserService) servletContext.getAttribute(ServiceParameters.USER_SERVICE);
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET request");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            List<BoxDocument> inboxDocumentList = new ArrayList<>();
            List<Document> documentList = documentService.getDocumentListByRecipientId(user.getId());
            if (documentList != null) {
                for (Document document : documentList) {
                    BoxDocument inboxDocument = new BoxDocument(document.getId());
                    inboxDocument.setFromParticipant(userService.getParticipant(document.getFromUserId()));
                    inboxDocument.setToParticipant(userService.getParticipant(document.getToUserId()));
                    inboxDocument.setDocumentName(document.getDocumentName());
                    inboxDocument.setSendDate(document.getSendDate());
                    inboxDocumentList.add(inboxDocument);
                }
            }
            request.setAttribute("inboxDocuments", inboxDocumentList);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__INDEX);
            dispatcher.forward(request, response);
            if (log.isDebugEnabled()) {
                log.debug("Response was sent");
            }
        } catch (DAOException e) {
            log.error("Server error", e);
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
    }
}
