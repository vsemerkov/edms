package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.certificate.CertParams;
import ua.nure.doc.server.certificate.CertificateType;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.entity.Certificate;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.service.CertServerInfoService;
import ua.nure.doc.server.service.CertificateService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;


public class GetCertificatesServlet extends HttpServlet {
    private CertificateService certificateService;
    private CertServerInfoService certServerInfoService;
    private static final Logger log = Logger.getLogger(GetCertificatesServlet.class);

    public GetCertificatesServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        certificateService = (CertificateService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVICE);
        if (certificateService == null) {
            log.error("Certificate service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate service attribute is not exists.");
        }
        certServerInfoService = (CertServerInfoService) servletContext.getAttribute(ServiceParameters.CERTIFICATE_SERVER_INFO_SERVICE);
        if (certServerInfoService == null) {
            log.error("Certificate server info service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate server info service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method started");
        }
        long userId = 0;
        CertificateType type = null;
        try {
            if (request.getParameterMap().containsKey(CertParams.PARAMETER__USER_ID)) {
                userId = Long.valueOf(request.getParameter(CertParams.PARAMETER__USER_ID));
            }
            if (request.getParameterMap().containsKey(CertParams.PARAMETER__CERT_TYPE)) {
                type = CertificateType.valueOf(request.getParameter(CertParams.PARAMETER__CERT_TYPE));
            }
            if (userId <= 0 && type != null) {
                throw new IllegalArgumentException("Incorrect request parameters");
            }
            List<CertServerInfo> certServerInfoList = certServerInfoService.getCertServerInfoList();
            Certificate certificate = certificateService.getCertificate(certServerInfoList, userId, type);
            JSONObject resultJson = new JSONObject();
            if (certificate != null) {
                resultJson.put(CertParams.PARAMETER__RESULT, certificate.getValue());
            }
            response.setContentType("text/json");
            PrintWriter writer = response.getWriter();
            writer.write(resultJson.toString());
            writer.flush();
            if (log.isDebugEnabled()) {
                log.debug("JSON was sent");
            }
        } catch (Exception e) {
            log.error("Server error", e);
            response.setStatus(400);
        }
    }
}
