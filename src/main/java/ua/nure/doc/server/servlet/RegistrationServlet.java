package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.certificate.CertificateType;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.convert.FormBeanConverter;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.FormBean;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.security.captcha.manager.CaptchaManager;
import ua.nure.doc.server.service.CertServerInfoService;
import ua.nure.doc.server.service.CertificateService;
import ua.nure.doc.server.service.UserService;
import ua.nure.doc.server.validator.Validator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class <code>RegistrationServlet</code>
 */
public class RegistrationServlet extends HttpServlet {
    private Validator validator = new Validator();
    private UserService userService;
    private CertServerInfoService certServerInfoService;
    private CertificateService certificateService;
    private CaptchaManager captchaManager;
    private static final Logger log = Logger
            .getLogger(RegistrationServlet.class);

    public RegistrationServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        userService = (UserService) servletContext
                .getAttribute(ServiceParameters.USER_SERVICE);
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
        certServerInfoService = (CertServerInfoService) servletContext
                .getAttribute(ServiceParameters.CERTIFICATE_SERVER_INFO_SERVICE);
        if (certServerInfoService == null) {
            log.error("Certificate server info service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate server info service attribute is not exists.");
        }
        certificateService = (CertificateService) servletContext
                .getAttribute(ServiceParameters.CERTIFICATE_SERVICE);
        if (certificateService == null) {
            log.error("Certificate service attribute is not exists.");
            throw new IllegalStateException(
                    "Certificate service attribute is not exists.");
        }
        captchaManager = (CaptchaManager) servletContext
                .getAttribute(ServiceParameters.CAPTCHA__MANAGER);
        if (captchaManager == null) {
            log.error("Captcha manager attribute is not exists.");
            throw new IllegalStateException(
                    "Captcha manager attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET request");
        }
        setCaptchaId(request, response);
        RequestDispatcher dispatcher = request
                .getRequestDispatcher(Path.PAGE__SIGN_UP);
        dispatcher.forward(request, response);
        if (log.isDebugEnabled()) {
            log.debug("Response was sended");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method started");
        }
        FormBean formBean = getFormBean(request);
        Map<String, String> paramErrors = validator.validateFormBean(formBean);
        boolean positiveResponse = true;
        if (!captchaManager.validateCaptcha(request)) {
            paramErrors
                    .put(UserParameters.USER__CAPTCHA,
                            "Captcha length is not equals to 5 or captcha lifetime is out!");
            log.trace("Captcha length is not equals to 5 or captcha lifetime is out!");
            positiveResponse = false;
        }
        if (paramErrors.size() != 0 || !positiveResponse) {
            positiveResponse = false;
        }
        if (positiveResponse) {
            User user = FormBeanConverter.getUser(formBean);
            boolean isContains = false;
            try {
                isContains = userService.containsUser(user);
            } catch (DAOException e) {
                log.error(e.getMessage());
                response.sendRedirect(Path.PAGE__ERROR_PAGE);
                return;
            }
            if (isContains == false) {
                if (log.isTraceEnabled()) {
                    log.trace("User " + user.getEmail()
                            + " not exists in database");
                }
                if (positiveResponse) {
                    try {
                        userService.add(user);
                        user = userService.get(user.getEmail());
                        if (log.isTraceEnabled()) {
                            log.trace("User " + user.getEmail()
                                    + " was added in database");
                        }
                        List<CertServerInfo> certServerInfoList = certServerInfoService.getCertServerInfoList();
                        positiveResponse = certificateService.add(certServerInfoList, user.getId(), CertificateType.DH, formBean.getDHPublicKey());
                        positiveResponse = positiveResponse && certificateService.add(certServerInfoList, user.getId(), CertificateType.CURVE, formBean.getCurvePublicKey());
                        if (!positiveResponse) {
                            throw new IllegalStateException("Public keys are not added");
                        }
                        response.sendRedirect(Path.COMMAND__INDEX);
                        if (log.isDebugEnabled()) {
                            log.debug("Response was sent");
                        }
                        return;
                    } catch (DAOException e) {
                        paramErrors.put(UserParameters.USER, "Server error");
                        positiveResponse = false;
                        log.error("User adding error", e);
                    }
                }
            } else {
                if (log.isTraceEnabled()) {
                    log.trace("User " + user.getEmail()
                            + " has existed in database");
                }
                paramErrors
                        .put(UserParameters.USER,
                                "User with such email address already exists in the system!");
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Parameters not validate");
        }
        request.setAttribute("formBean", formBean);
        request.setAttribute("paramErrors", paramErrors);
        setCaptchaId(request, response);
        RequestDispatcher dispatcher = request
                .getRequestDispatcher(Path.PAGE__SIGN_UP);
        dispatcher.forward(request, response);
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    /**
     * Get <code>FormBean</code> object from request.
     *
     * @param request
     * @return <code>FormBean</code> object.
     */
    private FormBean getFormBean(HttpServletRequest request) {
        FormBean formBean = new FormBean();
        formBean.setLastName(request
                .getParameter(UserParameters.USER__LAST_NAME));
        if (log.isTraceEnabled()) {
            log.trace("lastName --> " + formBean.getLastName());
        }
        formBean.setFirstName(request
                .getParameter(UserParameters.USER__FIRST_NAME));
        if (log.isTraceEnabled()) {
            log.trace("firstName --> " + formBean.getFirstName());
        }
        formBean.setPatronymic(request
                .getParameter(UserParameters.USER__PATRONYMIC));
        if (log.isTraceEnabled()) {
            log.trace("patronymic --> " + formBean.getPatronymic());
        }
        formBean.setEmail(request.getParameter(UserParameters.USER__EMAIL));
        if (log.isTraceEnabled()) {
            log.trace("email --> " + formBean.getEmail());
        }
        formBean.setPassword(request
                .getParameter(UserParameters.USER__PASSWORD));
        formBean.setPasswordRepeat(request
                .getParameter(UserParameters.USER__PASSWORD_REPEAT));
        formBean.setDHPublicKey(request.getParameter(UserParameters.USER__DH_PUBLIC_KEY));
        formBean.setCurvePublicKey(request.getParameter(UserParameters.USER__CURVE_PUBLIC_KEY));
        return formBean;
    }

    private void setCaptchaId(HttpServletRequest request,
                              HttpServletResponse response) {
        captchaManager.setId(request, response);
    }
}
