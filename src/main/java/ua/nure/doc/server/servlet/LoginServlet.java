package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.security.LoginStatus;
import ua.nure.doc.server.service.UserService;
import ua.nure.doc.server.validator.Validator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
    private Validator validator = new Validator();
    private UserService userService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LoginServlet.class);

    public LoginServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        userService = (UserService) servletContext
                .getAttribute(ServiceParameters.USER_SERVICE);
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method started");
        }
        RequestDispatcher dispatcher = request
                .getRequestDispatcher(Path.PAGE__SIGN_IN);
        dispatcher.forward(request, response);
        if (log.isDebugEnabled()) {
            log.debug("Response was sent");
        }
    }

    /**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("POST method started");
        }
        String refererPage = request.getHeader("referer");
        if (log.isTraceEnabled()) {
            log.trace("refererPage --> " + refererPage);
        }
        String email = (String) request
                .getParameter(UserParameters.USER__LOGIN_EMAIL);
        if (log.isTraceEnabled()) {
            log.trace("email --> " + email);
        }
        String password = (String) request
                .getParameter(UserParameters.USER__LOGIN_PASSWORD);
        Map<String, String> paramErrors = validator.validateLoginData(email,
                password);
        boolean positiveResponse = true;
        if (paramErrors.size() != 0) {
            request.setAttribute(UserParameters.USER__LOGIN_ERROR, email);
            request.setAttribute("paramErrors", paramErrors);
            positiveResponse = false;
        }
        String loginError = null;
        LoginStatus status = null;
        try {
            status = userService.login(email, password);
            if (positiveResponse && status == LoginStatus.CORRECT) {
                HttpSession session = request.getSession();
                session.setAttribute(UserParameters.USER,
                        userService.get(email));
                if (refererPage != null && !refererPage.contains("login") && !refererPage.contains("logout.jsp")) {
                    response.sendRedirect(refererPage);
                } else {
                    response.sendRedirect(Path.COMMAND__INDEX);
                }
                if (log.isDebugEnabled()) {
                    log.debug("Response was sent");
                }
                return;
            } else {
                if (loginError == null) {
                    loginError = "Incorrect email or password!";
                }
                paramErrors.put(UserParameters.USER__LOGIN_ERROR, loginError);
                request.setAttribute("paramErrors", paramErrors);
                RequestDispatcher dispatcher = request
                        .getRequestDispatcher(Path.PAGE__SIGN_IN);
                dispatcher.forward(request, response);
                if (log.isDebugEnabled()) {
                    log.debug("Response was sent");
                }
            }
        } catch (DAOException e) {
            log.error(e.getMessage());
            paramErrors.put(UserParameters.USER__LOGIN_ERROR, "Server error");
            request.setAttribute("paramErrors", paramErrors);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__SIGN_IN);
            dispatcher.forward(request, response);
            if (log.isDebugEnabled()) {
                log.debug("Response was sent");
            }
        }
    }
}
