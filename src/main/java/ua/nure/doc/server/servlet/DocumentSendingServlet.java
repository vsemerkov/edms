package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import ua.nure.doc.server.Path;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.Participant;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.parameter.UserParameters;
import ua.nure.doc.server.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class <code>FileSendingServlet</code>
 */
public class DocumentSendingServlet extends HttpServlet {
    private UserService userService;
    private static final Logger log = Logger.getLogger(DocumentSendingServlet.class);

    public DocumentSendingServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        userService = (UserService) servletContext.getAttribute(ServiceParameters.USER_SERVICE);
        if (userService == null) {
            log.error("User service attribute is not exists.");
            throw new IllegalStateException(
                    "User service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET request");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        try {
            List<Participant> recipientList = userService.getRecipientList(user.getId());
            request.setAttribute("recipients", recipientList);
            RequestDispatcher dispatcher = request
                    .getRequestDispatcher(Path.PAGE__SEND_FILE);
            dispatcher.forward(request, response);
            if (log.isDebugEnabled()) {
                log.debug("Response was sent");
            }
        } catch (DAOException e) {
            log.error("Server error", e);
            response.sendRedirect(Path.PAGE__ERROR_PAGE);
        }
    }
}
