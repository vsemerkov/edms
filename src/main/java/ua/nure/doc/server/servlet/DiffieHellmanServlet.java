package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.AppParameters;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.parameter.ServiceParameters;
import ua.nure.doc.server.service.ParamService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Map;

/**
 * Servlet implementation class DiffieHellmanServlet
 */
public class DiffieHellmanServlet extends HttpServlet {
    private ParamService paramService;
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DiffieHellmanServlet.class);

    public DiffieHellmanServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        paramService = (ParamService) servletContext
                .getAttribute(ServiceParameters.PARAM_SERVICE);
        if (paramService == null) {
            log.error("Param service attribute is not exists.");
            throw new IllegalStateException(
                    "Param service attribute is not exists.");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method started");
        }
        boolean result = true;
        BigInteger p = null;
        BigInteger g = null;
        try {
            Map<String, BigInteger> paramMap = paramService.get();
            if (paramMap != null && !paramMap.isEmpty()) {
                p = paramMap.get(AppParameters.P);
                g = paramMap.get(AppParameters.G);
            } else {
                result = false;
            }
        } catch (DAOException e) {
            log.error(e.getMessage());
            result = false;
        } finally {
            JSONObject resultJson = new JSONObject();
            resultJson.put("result", result);
            if (result) {
                resultJson.put(AppParameters.P, p.toString());
                resultJson.put(AppParameters.G, g.toString());
            }
            response.setContentType("text/json");
            PrintWriter writer = response.getWriter();
            writer.write(resultJson.toString());
            writer.flush();
            if (log.isDebugEnabled()) {
                log.debug("JSON was sent");
            }
        }
    }
}
