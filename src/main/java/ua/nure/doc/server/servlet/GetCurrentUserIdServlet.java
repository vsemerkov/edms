package ua.nure.doc.server.servlet;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.parameter.UserParameters;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet for getting current user id.
 *
 * @author Volodymyr_Semerkov
 */
public class GetCurrentUserIdServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GetCurrentUserIdServlet.class);

    public GetCurrentUserIdServlet() {
        if (log.isDebugEnabled()) {
            log.debug("Servlet created");
        }
    }

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("GET method started");
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(UserParameters.USER);
        JSONObject resultJson = new JSONObject();
        resultJson.put("result", user.getId());
        response.setContentType("text/json");
        PrintWriter writer = response.getWriter();
        writer.write(resultJson.toString());
        writer.flush();
        if (log.isDebugEnabled()) {
            log.debug("JSON was sent");
        }
    }
}
