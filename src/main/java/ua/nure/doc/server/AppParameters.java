package ua.nure.doc.server;

/**
 * Application constants.
 *
 * @author Volodymyr_Semerkov
 */
public interface AppParameters {
    public static final String P = "p";
    public static final String G = "g";
    public static final String USER__ID = "userId";
    public static final String PUBLIC_KEY = "publicKey";
}
