package ua.nure.doc.server.service.thread;

import org.apache.log4j.Logger;
import ua.nure.doc.server.certificate.command.Command;
import ua.nure.doc.server.certificate.command.CommandInfo;
import ua.nure.doc.server.certificate.command.CommandResult;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.service.CertServerInfoService;
import ua.nure.doc.server.service.DeferredCommandService;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Manager for deferred command executing.
 *
 * @author Volodymyr_Semerkov
 */
public class DeferredCommandManager implements Runnable {
    private CertServerInfoService certServerInfoService;
    private DeferredCommandService deferredCommandService;
    private static final Logger log = Logger.getLogger(DeferredCommandManager.class);

    public DeferredCommandManager(CertServerInfoService certServerInfoService, DeferredCommandService deferredCommandService) {
        this.certServerInfoService = certServerInfoService;
        this.deferredCommandService = deferredCommandService;
    }

    @Override
    public void run() {
        try {
            List<CertServerInfo> certServerInfoList = certServerInfoService.getCertServerInfoList();
            for (CertServerInfo certServerInfo : certServerInfoList) {
                Set<CommandInfo> commandInfoSet = deferredCommandService.getCertCommandInfoSet(certServerInfo.getId());
                if (commandInfoSet != null) {
                    boolean executionResult = true;
                    Iterator<CommandInfo> it = commandInfoSet.iterator();
                    while (it.hasNext() && executionResult == true) {
                        CommandInfo commandInfo = it.next();
                        Command command = new Command(commandInfo);
                        Thread commandThread = new Thread(command);
                        commandThread.start();
                        commandThread.join();
                        CommandResult commandResult = command.getCommandResult();
                        if (commandResult != null && commandResult.isSuccess()) {
                            deferredCommandService.removeCertCommandInfo(commandInfo.getId());
                            executionResult = true;
                        } else {
                            executionResult = false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Cannot execute deferred commands", e);
        }
    }
}
