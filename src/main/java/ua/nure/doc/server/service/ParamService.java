package ua.nure.doc.server.service;

import org.apache.log4j.Logger;
import ua.nure.doc.server.dao.ParamDAO;
import ua.nure.doc.server.dao.UserDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcTransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionOperation;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.security.Hashing;
import ua.nure.doc.server.security.LoginStatus;

import java.math.BigInteger;
import java.util.Map;

/**
 * This class contains methods for actions with parameters data.
 *
 * @author Volodymyr_Semrkov
 */
public class ParamService {
    private ParamDAO paramDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(ParamService.class);

    public ParamService(ParamDAO paramDAO) {
        if (log.isDebugEnabled()) {
            log.debug("Param service created");
        }
        this.paramDAO = paramDAO;
        this.transactionManager = new JdbcTransactionManager();
    }

    /**
     * Get public parameters.
     *
     * @return Public parameters P and G for Diffie-Hellman algorithm
     * @throws DAOException
     */
    public Map<String, BigInteger> get() throws DAOException {
        return (Map<String, BigInteger>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return paramDAO.getParams();
                    }
                });
    }

    /**
     * Add parameters data to database.
     *
     * @param p Parameter P.
     * @param g Parameter G.
     * @return True if parameters are added to database.
     * @throws DAOException
     */
    public boolean add(BigInteger p, BigInteger g) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return paramDAO.addParams(p, g);
                    }
                });
    }

    /**
     * Find parameters data in database.
     *
     * @return True if parameters data not contains in database, else False.
     * @throws DAOException
     */
    public boolean contains() throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return paramDAO.containsParams();
                    }
                });
    }
}
