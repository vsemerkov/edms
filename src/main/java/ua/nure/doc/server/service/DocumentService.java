package ua.nure.doc.server.service;

import org.apache.log4j.Logger;
import ua.nure.doc.server.dao.DocumentDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcTransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionOperation;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.Document;

import java.util.List;

/**
 * This class contains methods for actions with documents.
 *
 * @author Volodymyr_Semrkov
 */
public class DocumentService {
    private DocumentDAO documentDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(DocumentService.class);

    public DocumentService(DocumentDAO documentDAO) {
        this.documentDAO = documentDAO;
        this.transactionManager = new JdbcTransactionManager();
        if (log.isDebugEnabled()) {
            log.debug("Document service created");
        }
    }

    public boolean addDocument(Document document) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.addDocument(document);
            }
        });
    }

    public Document getDocument(long documentId) throws DAOException {
        return (Document) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.getDocument(documentId);
            }
        });
    }

    public boolean removeDocument(long documentId) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.removeDocument(documentId);
            }
        });
    }

    public boolean removeDocumentsBySenderId(long senderId) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.removeDocumentsBySenderId(senderId);
            }
        });
    }

    public boolean removeDocumentsByRecipientId(long recipientId) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.removeDocumentsByRecipientId(recipientId);
            }
        });
    }

    public List<Document> getDocumentListBySenderId(long senderId) throws DAOException {
        return (List<Document>) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.getDocumentListBySenderId(senderId);
            }
        });
    }

    public List<Document> getDocumentListByRecipientId(long recipientId) throws DAOException {
        return (List<Document>) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return documentDAO.getDocumentListByRecipientId(recipientId);
            }
        });
    }
}
