package ua.nure.doc.server.service;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import ua.nure.doc.server.certificate.CertParams;
import ua.nure.doc.server.certificate.CertificateType;
import ua.nure.doc.server.certificate.QueryStringBuilder;
import ua.nure.doc.server.certificate.command.Command;
import ua.nure.doc.server.certificate.command.CommandType;
import ua.nure.doc.server.certificate.command.CommandInfo;
import ua.nure.doc.server.certificate.command.CommandResult;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.convert.CertificateConverter;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.Certificate;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * This class contains methods for actions with certificates.
 *
 * @author Volodymyr_Semrkov
 */
public class CertificateService {
    private DeferredCommandService deferredCommandService;
    private static final Logger log = Logger.getLogger(CertificateService.class);

    public CertificateService(DeferredCommandService deferredCommandService) {
        this.deferredCommandService = deferredCommandService;
        if (log.isDebugEnabled()) {
            log.debug("Certificate service created");
        }
    }

    public boolean add(List<CertServerInfo> certServerInfoList, long userId, CertificateType type, String cert) throws DAOException {
        Map<String, String> paramMap = new LinkedHashMap<>();
        paramMap.put(CertParams.PARAMETER__USER_ID, String.valueOf(userId));
        paramMap.put(CertParams.PARAMETER__CERT_TYPE, type.toString());
        paramMap.put(CertParams.PARAMETER__CERT, cert);
        String queryString = QueryStringBuilder.makeQueryString(paramMap);
        Object result = execute(certServerInfoList, CommandType.ADD, CertParams.REQUEST_METHOD_POST, queryString);
        if (result != null) {
            return (boolean) result;
        }
        return false;
    }

    public Certificate getCertificate(List<CertServerInfo> certServerInfoList, long userId, CertificateType type) throws DAOException {
        Certificate certificate = null;
        Map<String, String> paramMap = new LinkedHashMap<>();
        paramMap.put(CertParams.PARAMETER__USER_ID, String.valueOf(userId));
        paramMap.put(CertParams.PARAMETER__CERT_TYPE, String.valueOf(type.toString()));
        String queryString = QueryStringBuilder.makeQueryString(paramMap);
        JSONArray array = (JSONArray) execute(certServerInfoList, CommandType.GET, CertParams.REQUEST_METHOD_GET, queryString);
        if (array != null && array.get(0) != JSONObject.NULL) {
            certificate = CertificateConverter.getCertificate(array.getJSONObject(0));
        }
        return certificate;
    }

    public boolean remove(List<CertServerInfo> certServerInfoList, long userId, long certId) throws DAOException {
        Map<String, String> paramMap = new LinkedHashMap<>();
        paramMap.put(CertParams.PARAMETER__CERT_ID, String.valueOf(certId));
        paramMap.put(CertParams.PARAMETER__USER_ID, String.valueOf(userId));
        String queryString = QueryStringBuilder.makeQueryString(paramMap);
        Object result = execute(certServerInfoList, CommandType.REMOVE, CertParams.REQUEST_METHOD_POST, queryString);
        if (result != null) {
            return (boolean) result;
        }
        return false;
    }

    public boolean revoke(List<CertServerInfo> certServerInfoList, String revocationServerPath, long userId, long certId) throws DAOException, InterruptedException {
        boolean result;
        Date revokeDate = null;
        Map<String, String> paramMap = new LinkedHashMap<>();
        paramMap.put(CertParams.PARAMETER__CERT_ID, String.valueOf(certId));
        String queryString = QueryStringBuilder.makeQueryString(paramMap);
        Object res = execute(certServerInfoList, CommandType.REVOKE, CertParams.REQUEST_METHOD_POST, queryString);
        result = res == null ? false : (boolean) res;
        Certificate certificate = null;
        if (result) {
            revokeDate = new Date();
            JSONArray array = (JSONArray) execute(certServerInfoList, CommandType.GET_REVOKED, CertParams.REQUEST_METHOD_GET, queryString);
            if (array != null) {
                certificate = CertificateConverter.getCertificate(array.getJSONObject(0));
                result = true;
            } else {
                result = false;
            }
        }
        if (result) {
            result = addToRevocationServer(revocationServerPath, userId, revokeDate, certificate);
        }
        if (result) {
            result = remove(certServerInfoList, userId, certId);
        }
        return result;
    }

    public boolean unrevoke(List<CertServerInfo> certServerInfoList, long certId) throws DAOException, InterruptedException {
        boolean result;
        Map<String, String> paramMap = new LinkedHashMap<>();
        paramMap.put(CertParams.PARAMETER__CERT_ID, String.valueOf(certId));
        String queryString = QueryStringBuilder.makeQueryString(paramMap);
        Object res = execute(certServerInfoList, CommandType.UNREVOKE, CertParams.REQUEST_METHOD_POST, queryString);
        result = res == null ? false : (boolean) res;
        return result;
    }

    private boolean addToRevocationServer(String revocationServerPath, long userId, Date revokeDate, Certificate certificate) {
        boolean result;
        Map<String, String> paramMap = new LinkedHashMap<>();
        paramMap.put(CertParams.PARAMETER__USER_ID, String.valueOf(userId));
        paramMap.put(CertParams.PARAMETER__REVOKE_DATE, String.valueOf(revokeDate.getTime()));
        paramMap.put(CertParams.PARAMETER__CERT, certificate.getValue());
        paramMap.put(CertParams.PARAMETER__CERT_TYPE, certificate.getType().toString());
        String queryString = QueryStringBuilder.makeQueryString(paramMap);
        CommandInfo commandInfo = new CommandInfo(revocationServerPath, CommandType.ADD, CertParams.REQUEST_METHOD_POST, queryString, new Date().getTime());
        Command command = new Command(commandInfo);
        Thread commandThread = new Thread(command);
        commandThread.start();
        try {
            commandThread.join();
            CommandResult commandResult = command.getCommandResult();
            result = commandResult != null && commandResult.isSuccess();
        } catch (InterruptedException e) {
            result = false;
        }
        return result;
    }

    private Object execute(List<CertServerInfo> certServerInfoList, CommandType commandType, String requestMethod, String queryString) throws DAOException {
        if (certServerInfoList != null && !certServerInfoList.isEmpty()) {
            List<Command> commandList = new ArrayList<>();
            ExecutorService executorService = Executors.newFixedThreadPool(certServerInfoList.size());
            long timestamp = new Date().getTime();
            for (CertServerInfo serverInfo : certServerInfoList) {
                CommandInfo commandInfo = new CommandInfo(serverInfo.getURLPath(), commandType, requestMethod, queryString, timestamp);
                if (!deferredCommandService.containsCommandsForServer(serverInfo.getId())) {
                    Command command = new Command(commandInfo);
                    executorService.submit(command);
                    commandList.add(command);
                } else {
                    if (requestMethod.equals(CertParams.REQUEST_METHOD_POST)) {
                        deferredCommandService.addCertCommandInfo(commandInfo, serverInfo.getId());
                    }
                }
            }
            Object totalResult = null;
            if (!commandList.isEmpty()) {
                executorService.shutdown();
                try {
                    executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    log.error("Cannot await certificate commands termination", e);
                }
                for (Command command : commandList) {
                    CommandResult commandResult = command.getCommandResult();
                    boolean isSuccess = commandResult != null && commandResult.isSuccess();
                    if (requestMethod.equals(CertParams.REQUEST_METHOD_GET)) {
                        if (isSuccess) {
                            return commandResult.getResult();
                        }
                    }
                    if (requestMethod.equals(CertParams.REQUEST_METHOD_POST)) {
                        if (isSuccess) {
                            totalResult = true;
                        } else {
                            CommandInfo commandInfo = command.getCommandInfo();
                            CertServerInfo serverInfo = getServerInfoByURL(certServerInfoList, commandInfo.getURLPath());
                            deferredCommandService.addCertCommandInfo(commandInfo, serverInfo.getId());
                        }
                    }
                }
            } else {
                log.error("All certificate servers are unavailable");
            }
            return totalResult;
        } else {
            log.error("There are not certificate servers");
            throw new IllegalStateException("There are not certificate servers");
        }
    }

    private CertServerInfo getServerInfoByURL(List<CertServerInfo> certServerInfoList, String URLPath) {
        for (CertServerInfo certServerInfo : certServerInfoList) {
            if (certServerInfo.getURLPath().equals(URLPath)) {
                return certServerInfo;
            }
        }
        return null;
    }
}
