package ua.nure.doc.server.service;

import org.apache.log4j.Logger;
import ua.nure.doc.server.certificate.command.CommandInfo;
import ua.nure.doc.server.dao.DeferredCommandDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcTransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionOperation;
import ua.nure.doc.server.dao.exception.DAOException;

import java.util.Set;

/**
 * This class contains methods for actions with certificate commands data.
 *
 * @author Volodymyr_Semrkov
 */
public class DeferredCommandService {
    private DeferredCommandDAO deferredCommandDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(DeferredCommandService.class);

    public DeferredCommandService(DeferredCommandDAO deferredCommandDAO) {
        this.deferredCommandDAO = deferredCommandDAO;
        this.transactionManager = new JdbcTransactionManager();
        if (log.isDebugEnabled()) {
            log.debug("Deferred command service created");
        }
    }

    public boolean addCertCommandInfo(CommandInfo commandInfo, long certServerId) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return deferredCommandDAO.addCertCommandInfo(commandInfo, certServerId);
            }
        });
    }

    public boolean removeCertCommandInfo(long id) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return deferredCommandDAO.removeCertCommandInfo(id);
            }
        });
    }

    public Set<CommandInfo> getCertCommandInfoSet(long certServerId) throws DAOException {
        return (Set<CommandInfo>) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return deferredCommandDAO.getCertCommandInfoSet(certServerId);
            }
        });
    }

    public boolean containsCommandsForServer(long certServerId) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return deferredCommandDAO.containsCommandsForServer(certServerId);
            }
        });
    }
}
