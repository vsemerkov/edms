package ua.nure.doc.server.service;

import org.apache.log4j.Logger;
import ua.nure.doc.server.certificate.entity.CertServerInfo;
import ua.nure.doc.server.dao.CertServerInfoDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcTransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionOperation;
import ua.nure.doc.server.dao.exception.DAOException;

import java.util.List;

/**
 * This class contains methods for actions with certificate servers data.
 *
 * @author Volodymyr_Semrkov
 */
public class CertServerInfoService {
    private CertServerInfoDAO certServerInfoDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(CertServerInfoService.class);

    public CertServerInfoService(CertServerInfoDAO certServerInfoDAO, List<CertServerInfo> newCertServerInfoList) {
        this.certServerInfoDAO = certServerInfoDAO;
        this.transactionManager = new JdbcTransactionManager();
        try {
            removeUnusedCertServerInfo(newCertServerInfoList);
            prepareCertServerInfoList(newCertServerInfoList);
            if (log.isDebugEnabled()) {
                log.debug("Certificate server info service created");
            }
        } catch (DAOException e) {
            log.error("Cannot create service", e);
            throw new IllegalStateException("Cannot create service");
        }
    }

    public boolean addCertServerInfo(CertServerInfo certServerInfo) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return certServerInfoDAO.addCertServerInfo(certServerInfo);
            }
        });
    }

    public boolean removeCertServerInfo(long id) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return certServerInfoDAO.removeCertServerInfo(id);
            }
        });
    }

    public boolean removeCertServerInfo(String url) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return certServerInfoDAO.removeCertServerInfo(url);
            }
        });
    }

    public CertServerInfo getCertServerInfo(String url) throws DAOException {
        return (CertServerInfo) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return certServerInfoDAO.getCertServerInfo(url);
            }
        });
    }

    public List<CertServerInfo> getCertServerInfoList() throws DAOException {
        return (List<CertServerInfo>) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return certServerInfoDAO.getCertServerInfoList();
            }
        });
    }

    public boolean containsCertServerInfo(String url) throws DAOException {
        return (boolean) transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                return certServerInfoDAO.containsCertServerInfo(url);
            }
        });
    }

    private void removeUnusedCertServerInfo(List<CertServerInfo> newCertServerInfoList) throws DAOException {
        List<CertServerInfo> databaseCertServerInfoList = getCertServerInfoList();
        if (databaseCertServerInfoList != null && !databaseCertServerInfoList.isEmpty()) {
            databaseCertServerInfoList.removeAll(newCertServerInfoList);
            for (CertServerInfo certServerInfo : databaseCertServerInfoList) {
                removeCertServerInfo(certServerInfo.getId());
            }
        }
    }

    private List<CertServerInfo> prepareCertServerInfoList(List<CertServerInfo> newCertServerInfoList) throws DAOException {
        for (CertServerInfo certServerInfo : newCertServerInfoList) {
            if (!containsCertServerInfo(certServerInfo.getURLPath())) {
                addCertServerInfo(certServerInfo);
            }
        }
        return getCertServerInfoList();
    }
}
