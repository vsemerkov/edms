package ua.nure.doc.server.service;

import org.apache.log4j.Logger;
import ua.nure.doc.server.dao.UserDAO;
import ua.nure.doc.server.dao.db.jdbc.JdbcTransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionManager;
import ua.nure.doc.server.dao.db.transaction.TransactionOperation;
import ua.nure.doc.server.dao.exception.DAOException;
import ua.nure.doc.server.entity.Participant;
import ua.nure.doc.server.entity.User;
import ua.nure.doc.server.security.Hashing;
import ua.nure.doc.server.security.LoginStatus;

import java.util.List;

/**
 * This class contains methods for actions with users data.
 *
 * @author Volodymyr_Semrkov
 */
public class UserService {
    private UserDAO userDAO;
    private TransactionManager transactionManager;
    private static final Logger log = Logger.getLogger(UserService.class);

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
        this.transactionManager = new JdbcTransactionManager();
        if (log.isDebugEnabled()) {
            log.debug("User service created");
        }
    }

    /**
     * Find user data in database.
     *
     * @param user User data.
     * @return True if user data contains in database, else False.
     * @throws DAOException
     */
    public boolean containsUser(final User user) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.containsUser(user.getEmail());
                    }
                });
    }

    /**
     * Find user data in database.
     *
     * @param email User email.
     * @return True if user data contains in database, else False.
     * @throws DAOException
     */
    public boolean containsUser(final String email) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.containsUser(email);
                    }
                });
    }

    /**
     * User authentication.
     *
     * @param email    User email.
     * @param password User password.
     * @return
     * @throws DAOException
     */
    public boolean authenticate(final String email, final String password)
            throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        User user = userDAO.getUser(email);
                        if (user == null)
                            return false;
                        return user.getPassword().equals(Hashing.salt(password, email));
                    }
                });
    }

    /**
     * Add user data to database.
     *
     * @param user User data.
     * @return True if user data not contains in database, else False.
     * @throws DAOException
     */
    public boolean add(final User user) throws DAOException {
        return (boolean) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        String password = user.getPassword();
                        String email = user.getEmail();
                        user.setPassword(Hashing.salt(password, email));
                        return userDAO.addUser(user);
                    }
                });
    }

    /**
     * Get user by email.
     *
     * @param email User email.
     * @return User object if user contains in database, else {@code null}.
     * @throws DAOException
     */
    public User get(final String email) throws DAOException {
        return (User) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.getUser(email);
                    }
                });
    }

    /**
     * Get user by id.
     *
     * @param userId User ID.
     * @return User object if user contains in database, else {@code null}.
     * @throws DAOException
     */
    public User get(final long userId) throws DAOException {
        return (User) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.getUser(userId);
                    }
                });
    }

    /**
     * Remove user from database.
     *
     * @param email User email.
     * @return True if user data was removed from database, else False.
     * @throws DAOException
     */
    public void remove(final String email) throws DAOException {
        transactionManager.doInTransaction(new TransactionOperation() {
            @Override
            public Object execute() throws DAOException {
                userDAO.removeUser(email);
                return null;
            }
        });
    }

    /**
     * Check and set user locking parameters then user enters in the system from
     * login form.
     *
     * @param email    User email.
     * @param password User password.
     * @return
     * @throws DAOException
     */
    public LoginStatus login(final String email, final String password)
            throws DAOException {
        return (LoginStatus) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        User user = userDAO.getUser(email);
                        if (user == null) {
                            return LoginStatus.INCORRECT;
                        }
                        LoginStatus status = LoginStatus.CORRECT;
                        if (user.getPassword() == null
                                || !user.getPassword().equals(Hashing.salt(password, email))
                                || status != LoginStatus.CORRECT) {
                            status = LoginStatus.INCORRECT;
                        }
                        return status;
                    }
                });
    }

    /**
     * Get recipient list without current user.
     *
     * @param currentUserId Current user id.
     * @return Recipient list without current user.
     * @throws DAOException
     */
    public List<Participant> getRecipientList(final long currentUserId) throws DAOException {
        return (List<Participant>) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.getRecipientList(currentUserId);
                    }
                });
    }

    /**
     * Get participant by user id.
     *
     * @param userId User id.
     * @return Participant with appropriate user id.
     * @throws DAOException
     */
    public Participant getParticipant(long userId) throws DAOException {
        return (Participant) transactionManager
                .doInTransaction(new TransactionOperation() {
                    @Override
                    public Object execute() throws DAOException {
                        return userDAO.getParticipant(userId);
                    }
                });
    }
}
