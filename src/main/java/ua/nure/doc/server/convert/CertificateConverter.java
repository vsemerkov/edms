package ua.nure.doc.server.convert;

import org.json.JSONObject;
import ua.nure.doc.server.certificate.CertificateType;
import ua.nure.doc.server.entity.Certificate;
import ua.nure.doc.server.parameter.CertificateParameters;

/**
 * Certificate converter.
 *
 * @author Volodymyr_Semerkov
 */
public class CertificateConverter {

    public static Certificate getCertificate(JSONObject jsonObject) {
        Certificate certificate = new Certificate();
        certificate.setId(jsonObject.getLong(CertificateParameters.CERTIFICATE__ID));
        certificate.setUserId(jsonObject.getLong(CertificateParameters.CERTIFICATE__USER_ID));
        certificate.setValue(jsonObject.getString(CertificateParameters.CERTIFICATE__VALUE));
        certificate.setType(CertificateType.valueOf(jsonObject.getJSONObject(CertificateParameters.CERTIFICATE__TYPE).getString(CertificateParameters.CERTIFICATE__TYPE)));
        return certificate;
    }
}
