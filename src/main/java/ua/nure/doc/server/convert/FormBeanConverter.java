package ua.nure.doc.server.convert;

import ua.nure.doc.server.entity.FormBean;
import ua.nure.doc.server.entity.User;

import java.math.BigInteger;

/**
 * This class converts <code>FormBean</code> class object to
 * <code>UserInfo</code> class object.
 *
 * @author Volodymyr_Semerkov
 */
public class FormBeanConverter {
    /**
     * Converting <code>FormBean</code> class object to <code>UserInfo</code>
     * class object.
     *
     * @param formBean
     * @return
     */
    public static User getUser(FormBean formBean) {
        User user = new User(formBean.getLastName(), formBean.getFirstName(), formBean.getPatronymic(),
                formBean.getEmail(), formBean.getPassword());
        return user;
    }
}
