﻿$(document).ready(function () {

    $(function () {
        $('#secretFile').click(function () {
            $('input[name=filePassword]').val('');
            $('input[name=filePasswordRepeat]').val('');
            $('#secretFilePanel').addClass('invisible');
            $('button[name=changeKeys]').removeClass('invisible');
        });
    });

    $(function () {
        $("button[name=changeKeys]").click(function () {
            $.ajax({
                type: 'POST',
                url: 'changeKeys',
                data: {
                    dhPublicKey: $("#dhPublicKey").val(),
                    curvePublicKey: $("#curvePublicKey").val()
                },
                success: function (data) {
                    if (data["result"]) {
                        alert("Keys are changed!");
                        location.reload();
                    } else {
                        alert("Cannot change keys!");
                    }
                },
                error: function (data) {
                    alert("Cannot change keys!");
                }
            });
        });
    });

});
