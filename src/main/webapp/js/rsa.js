var RSA = {}

RSA.code = function (e, n, m) {
    return RSA.pow(m, e, n);
}

/*RSA.pow = function (base, exp, mod) {
    var i = 31;
    for (; i >= 0; i--) {
        if ((1 & (exp >>> i)) == 1) break;
    }
    if (i < 0) return 1;
    var res = base % mod;
    for (var j = i - 1; j >= 0; j--) {
        res = res * res % mod;
        if ((1 & (exp >>> j)) == 1) res = res * base % mod;
    }
    return res;

}*/

RSA.pow = function (base, exp, mod) {
    var res = 1;
    for(var i = 1; i <= exp; i++){
        res *= base;
        res %= mod;
    }
    return res;
}

RSA.isPrime = function (ch) {
    for (var i = 2; i <= Math.sqrt(ch); i++) {
        if (ch % i == 0)
            return false;
    }
    return true;
};

RSA.getArrayOfPrimeNumbers = function (size) {
    var primeArray = new Array(size);
    var count = 0;
    var number = 65537;
    while (count != size) {
        number += 2;
        if (RSA.isPrime(number)) {
            primeArray[count] = number;
            count++;
        }
    }
    return primeArray;
};

RSA.primeArray = RSA.getArrayOfPrimeNumbers(300);

RSA.generateKeys = function () {
    var rndNumber = Math.floor((Math.random() * 100));
    var p = RSA.primeArray[rndNumber + 100];
    rndNumber = Math.floor((Math.random() * 100));
    var q = RSA.primeArray[rndNumber + 100];
    while (p == q) {
        rndNumber = Math.floor((Math.random() * 100));
        q = RSA.primeArray[rndNumber + 100];
    }
    var n = p * q;
    var e = 0;
    var f = (p - 1) * (q - 1);
    for (var i = 6; i < RSA.primeArray.length; i++) {
        if (f % RSA.primeArray[i] != 0) {
            e = RSA.primeArray[i];
            break;
        }
    }
    var d = 1;
    do {
        d += 2;
    } while ((d * e) % f != 1);
    var keys = new Array();
    keys['e'] = e;
    keys['d'] = d;
    keys['n'] = n;
    console.log("Client p = " + p);
    console.log("Client q = " + q);
    console.log("Client e = " + e);
    console.log("Client d = " + d);
    console.log("Client n = " + n);
    return keys;
};
