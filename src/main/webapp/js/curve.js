var CURVE = {}

CURVE.PUBLIC_KEY = 'curvePublicKey';
CURVE.PRIVATE_KEY = 'curvePrivateKey';

CURVE.SIGNATURE_BEGIN = '\n-----SIGNATURE BEGIN-----\n';
CURVE.SIGNATURE_END = '\n-----SIGNATURE END-----\n';

CURVE.CURVE = 'secp256r1';
CURVE.SIGNATURE_ALGORITHM = 'SHA256withECDSA';
CURVE.PROV = 'cryptojs/jsrsa';

CURVE.getUserPublicKey = function (userId) {
    var recipientPublicKey;
    var responseText = $.ajax({
        type: 'GET',
        url: 'userCertificates',
        async: false,
        data: {
            userId: userId,
            certType: "CURVE"
        }
    }).responseText;
    if (responseText != "") {
        var map = JSON.parse(responseText);
        if (!map['result']) {
            alert('Error during receiving user public key!');
        } else {
            pemCurvePublicKey = atob(map['result']);
            recipientPublicKey = KEYUTIL.getHexFromPEM(pemCurvePublicKey, CURVE.PUBLIC_KEY);
        }
    }
    return recipientPublicKey;
}

CURVE.generateCurveKeyPair = function () {
    var curveKeyPair = [];
    var ec = new KJUR.crypto.ECDSA({'alg': CURVE.SIGNATURE_ALGORITHM, 'prov': CURVE.PROV});
    var keyPairHex = ec.generateKeyPairHex();
    curveKeyPair[CURVE.PRIVATE_KEY] = keyPairHex.ecprvhex;
    curveKeyPair[CURVE.PUBLIC_KEY] = keyPairHex.ecpubhex;
    return curveKeyPair;
}

CURVE.removeSignature = function (text) {
    return text.substring(0, text.lastIndexOf(CURVE.SIGNATURE_BEGIN));
}

CURVE.signText = function (text, curvePrivateKey) {
    var sig = new KJUR.crypto.Signature({"alg": CURVE.SIGNATURE_ALGORITHM, "prov": CURVE.PROV});
    sig.initSign({'ecprvhex': curvePrivateKey, 'eccurvename': CURVE.CURVE});
    sig.updateString(text);
    var signature = sig.sign();
    text += CURVE.SIGNATURE_BEGIN;
    text += signature;
    text += CURVE.SIGNATURE_END;
    return text;
}

CURVE.verifySignature = function (senderCurvePublicKey, text, signatureFromText) {
    var sig = new KJUR.crypto.Signature({"alg": CURVE.SIGNATURE_ALGORITHM, "prov": CURVE.PROV});
    sig.initVerifyByPublicKey({'ecpubhex': senderCurvePublicKey, 'eccurvename': CURVE.CURVE});
    sig.updateString(text);
    var result = sig.verify(signatureFromText);
    return result;
}

CURVE.getSignatureFromText = function (text) {
    var startPos = text.lastIndexOf(CURVE.SIGNATURE_BEGIN);
    startPos += CURVE.SIGNATURE_BEGIN.length;
    var endPos = text.lastIndexOf(CURVE.SIGNATURE_END);
    var signatureFromText = text.substring(startPos, endPos);
    return signatureFromText;
}
