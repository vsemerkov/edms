var DH = {}

DH.PUBLIC_KEY = 'DH_publicKey';
DH.SECREET_KEY = 'DH_secretKey';
DH.P = 'p';
DH.G = 'g';

DH.getInitParameters = function () {
    var initParams = [];
    var responseText = $.ajax({
        type: 'GET',
        url: 'initParams',
        async: false
    }).responseText;
    var map = JSON.parse(responseText);
    if (!map['result']) {
        alert('Error during receiving parameters P and G!');
    } else {
        initParams[DH.P] = map[DH.P];
        initParams[DH.G] = map[DH.G];
    }
    return initParams;
}

DH.generateKeyPair = function (p, g) {
    var rndNumber = Math.floor((Math.random() * 200));
    var secretKey = RSA.primeArray[rndNumber + 100];
    var publicKey = DH.getPublicKey(p, g, secretKey);
    var keyPair = [];
    keyPair[DH.PUBLIC_KEY] = publicKey;
    keyPair[DH.SECREET_KEY] = secretKey;
    return keyPair;
}

DH.getPublicKey = function (p, g, secretKey) {
    return RSA.pow(g, secretKey, p);
}

DH.getUserPublicKey = function (userId) {
    var recipientPublicKey;
    var responseText = $.ajax({
        type: 'GET',
        url: 'userCertificates',
        async: false,
        data: {
            userId: userId,
            certType: "DH"
        }
    }).responseText;
    if (responseText != "") {
        var map = JSON.parse(responseText);
        if (!map['result']) {
            alert('Error during receiving user public key!');
        } else {
            recipientPublicKey = map['result'];
        }
    }
    return recipientPublicKey;
}

DH.calculateCipherKey = function (otherPublicKey, dhSecretKey, p) {
    return RSA.pow(otherPublicKey, dhSecretKey, p);
}
