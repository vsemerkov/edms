$(document).ready(function () {

    var sendingFile = null;
    var secretFile = null;

    /**
     * Set document for sending
     */
    $(function () {
        $("#fileToEncryption").change(function (e) {
            if (e.target.files.length != 1) {
                alert('Please select a file to signing and encryption!');
                sendingFile = null;
                $("#fileToEncryption").val("");
                return;
            }
            sendingFile = e.target.files[0];
            if (sendingFile.size > 10 * 1024 * 1024) {
                alert('Please choose files smaller than 10 MB!');
                sendingFile = null;
                $("#fileToEncryption").val("");
                return;
            }
        });
    });

    /**
     * Set file with private keys
     */
    $(function () {
        $("#secretFile").change(function (e) {
            if (e.target.files.length != 1) {
                alert('Please select a file with keys!');
                secretFile = null;
                $("#secretFile").val("");
                return;
            }
            secretFile = e.target.files[0];
            if (secretFile.size > 1 * 1024 * 1024) {
                alert('File with keys is less than 1 MB!');
                secretFile = null;
                $("#fileToEncryption").val("");
                return;
            }
        });
    });

    $(function () {
        $("input[name=encryptButton]").click(function () {
            var recipientId = $("select[name=recipient]").val();
            if (recipientId <= 0) {
                alert("Recipient is not set!");
                return;
            }
            if (secretFile == null) {
                alert("File with keys is not set!");
                return;
            }
            if (sendingFile == null) {
                alert("File to sending is not set!");
                return;
            }
            $(this).prop('disabled', true);
            var dhSecretKey;
            var curvePrivateKey;
            var secretFileReader = new FileReader();
            secretFileReader.readAsText(secretFile, 'UTF-8');
            var sendingFileReader = new FileReader();

            secretFileReader.onload = function (e) {
                var privateKeys = SF.readFile(e);
                dhSecretKey = privateKeys[DH.SECREET_KEY];
                curvePrivateKey = privateKeys[CURVE.PRIVATE_KEY];
                sendingFileReader.readAsDataURL(sendingFile);
            };

            sendingFileReader.onload = function (e) {
                var text = e.target.result;
                var documentName = $("#fileToEncryption").val();
                text = CURVE.signText(text, curvePrivateKey);
                var recipientPublicKey = DH.getUserPublicKey(recipientId);
                var initParams = DH.getInitParameters();
                if (recipientPublicKey !== undefined) {
                    var cipherKey = DH.calculateCipherKey(recipientPublicKey, dhSecretKey, initParams[DH.P]);
                    var encryptedText = ENC.encryptText(text, cipherKey);
                    var currentUserId = UTIL.getCurrentUserId();
                    encryptedText = ENC.addUserId(encryptedText, currentUserId);
                    uploadDocument(recipientId, documentName, encryptedText);
                } else {
                    alert("Recipient public key is not existed!");
                }
                $("input[name=encryptButton]").prop('disabled', false);
                $("input[name=encryptButton]").blur();
            };
        });
    });

    function uploadDocument(recipientId, documentName, text) {
        $.ajax({
            type: 'POST',
            url: 'uploadDocument',
            contentType: 'multipart/form-data',
            data: {
                recipientId: recipientId,
                documentName: documentName,
                document: text
            },
            cache: false,
            success: function (data) {
                if (data["result"]) {
                    alert("Document is uploaded!");
                } else {
                    alert("Cannot upload the document!");
                }
            },
            error: function (data) {
                alert("Cannot upload the document!");
            }
        });
    }

    /**
     * Receive document
     */
    $(function () {
        $(".downloadButton").click(function () {
            var documentId = $(this).attr("id");
            if (secretFile == null) {
                alert("File with keys is not set!");
                return;
            } else {
                downloadDocument(documentId);
            }
        });
    });

    function downloadDocument(documentId) {
        $.ajax({
            type: 'GET',
            url: 'downloadDocument',
            data: {
                documentId: documentId
            },
            cache: false,
            success: function (data) {
                var documentName = data["documentName"];
                var res = data["document"];
                var senderId = ENC.getSenderId(res);
                res = ENC.removeUserId(res);
                var initParams = DH.getInitParameters();
                var senderDHPublicKey = DH.getUserPublicKey(senderId);
                var senderCurvePublicKey = CURVE.getUserPublicKey(senderId);
                if (senderDHPublicKey !== undefined && senderCurvePublicKey !== undefined) {
                    var dhSecretKey;
                    var secretFileReader = new FileReader();
                    secretFileReader.readAsText(secretFile, 'UTF-8');
                    secretFileReader.onload = function (e) {
                        var privateKeys = SF.readFile(e);
                        dhSecretKey = privateKeys[DH.SECREET_KEY];
                        var cipherKey = DH.calculateCipherKey(senderDHPublicKey, dhSecretKey, initParams[DH.P]);
                        var decryptedText = ENC.decryptText(res, cipherKey);
                        decryptedText = decryptedText.toString(CryptoJS.enc.Utf8);
                        var signatureFromText = CURVE.getSignatureFromText(decryptedText);
                        decryptedText = CURVE.removeSignature(decryptedText);
                        var result = CURVE.verifySignature(senderCurvePublicKey, decryptedText, signatureFromText);
                        if (!result) {
                            alert("Error! Document has invalid signature!");
                        } else {
                            alert("Right signature!");
                            var link = document.createElement('a');
                            var linkText = document.createTextNode(documentName);
                            link.appendChild(linkText);
                            link.id = documentId;
                            link.title = documentName;
                            link.href = decryptedText;
                            link.download = documentName;
                            var downloadCell = $("#" + documentId + "[name=download]");
                            downloadCell.empty();
                            downloadCell.append(link);
                            link.click();
                        }
                    };
                } else {
                    alert("Sender public key is not existed!");
                }
            },
            error: function (data) {
                alert("Cannot download the document!");
            }
        });
    }

    /**
     * Remove document
     */
    $(function () {
        $(".removeButton").click(function () {
            var documentId = $(this).attr("id");
            $.ajax({
                type: 'POST',
                url: 'removeDocument',
                data: {
                    documentId: documentId
                },
                success: function (data) {
                    if (data["result"]) {
                        alert("Document is removed!");
                        location.reload();
                    } else {
                        alert("Cannot remove the document!");
                    }
                },
                error: function (data) {
                    alert("Cannot remove the document!");
                }
            });
        });
    });

});
