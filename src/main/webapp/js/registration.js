$(document).ready(function () {

    $(function () {
        $('button[name=downloadSecretFile]').click(function () {
            $('span[id=filePassword]').text('');
            $('span[id=filePasswordRepeat]').text('');
            var filePassword = $('input[name=filePassword]').val().trim();
            var filePasswordRepeat = $('input[name=filePasswordRepeat]').val().trim();
            if (filePassword.length >= 8) {
                if (filePassword !== filePasswordRepeat) {
                    $('span[id=filePasswordRepeat]').text('Passwords are not equal!');
                } else {
                    var initParameters = DH.getInitParameters();
                    var keyPair = DH.generateKeyPair(initParameters[DH.P], initParameters[DH.G]);
                    var curveKeyPair = CURVE.generateCurveKeyPair();
                    var curvePrivateKey = curveKeyPair[CURVE.PRIVATE_KEY];
                    var curvePublicKey = curveKeyPair[CURVE.PUBLIC_KEY];
                    var pemCurvePrivateKey = KJUR.asn1.ASN1Util.getPEMStringFromHex(curvePrivateKey, CURVE.PRIVATE_KEY);
                    var pemCurvePublicKey = KJUR.asn1.ASN1Util.getPEMStringFromHex(curvePublicKey, CURVE.PUBLIC_KEY);
                    pemCurvePublicKey = btoa(pemCurvePublicKey);
                    $("#dhPublicKey").val(numberToString(keyPair[DH.PUBLIC_KEY]));
                    $("#curvePublicKey").val(pemCurvePublicKey);
                    var obj = {
                        'DH_secretKey': keyPair[DH.SECREET_KEY],
                        'curvePrivateKey': pemCurvePrivateKey
                    };
                    var text = JSON.stringify(obj);
                    var encryptedText = CryptoJS.AES.encrypt(text, filePassword);
                    $(this).addClass('invisible');
                    SF.writeFile(encryptedText);
                }
            } else {
                $('span[id=filePassword]').text('Password must contains more than 8 characters!');
            }
        });
    });

    $(function () {
        $('#secretFile').click(function () {
            $('input[name=filePassword]').val('');
            $('input[name=filePasswordRepeat]').val('');
            $('#secretFilePanel').addClass('invisible');
            $('button[name=registration]').removeClass('invisible');
        });
    });

    function numberToString(number) {
        var str = "";
        while (number != 0) {
            var ch = number % 10;
            number -= ch;
            number /= 10;
            str = ch + str;
        }
        return str;
    }

});