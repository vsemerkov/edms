var SF = {}

SF.writeFile = function (encryptedText) {
    var date = new Date();
    var fileName = 'EDMS_secret_' + date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + '_' + date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds() + '.dat';
    var link = $('#secretFile');
    link.text(fileName);
    link.attr('href', 'data:application/octet-stream,' + encryptedText);
    link.attr('download', fileName);
}

SF.readFile = function (e) {
    var privateKeys = [];
    var encryptedText = e.target.result;
    var password = $('#filePassword').val().trim();
    try {
        var text = CryptoJS.AES.decrypt(encryptedText, password).toString(CryptoJS.enc.Utf8);
        if (text.includes(DH.SECREET_KEY) && text.includes(CURVE.PRIVATE_KEY)) {
            var map = JSON.parse(text);
            var dhSecretKey = map[DH.SECREET_KEY];
            var pemCurvePrivateKey = map[CURVE.PRIVATE_KEY];
            privateKeys[DH.SECREET_KEY] = dhSecretKey;
            privateKeys[CURVE.PRIVATE_KEY] = KEYUTIL.getHexFromPEM(pemCurvePrivateKey, CURVE.PRIVATE_KEY);
        } else {
            alert('Invalid password or file with keys!');
        }
    } catch (e) {
        alert('Invalid password or file with keys!');
    }
    $("input[name=encryptButton]").prop('disabled', false);
    $("input[name=encryptButton]").blur();
    return privateKeys;
}
