var UTIL = {}

UTIL.getCurrentUserId = function () {
    var currentUserId;
    var responseText = $.ajax({
        type: 'GET',
        url: 'currentUserId',
        async: false
    }).responseText;
    var map = JSON.parse(responseText);
    if (!map['result']) {
        alert('Error during receiving current user id parameter!');
    } else {
        currentUserId = map['result'];
    }
    return currentUserId;
}
