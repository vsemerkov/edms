var ENC = {}

ENC.USER_ID_BEGIN = '\n-----USER ID BEGIN-----\n';
ENC.USER_ID_END = '\n-----USER ID END-----\n';

ENC.numberToWordArray = function (number) {
    var words = [];
    var i = 0;
    while (number != 0) {
        var word = number % 0x100000000;
        number -= word;
        number /= 0x100000000;
        words.push(word);
        i++;
    }
    var nBytes = i * 4;
    return new CryptoJS.lib.WordArray.init(words, nBytes);
}

ENC.encryptText = function (fileText, cipherKey) {
    var key = ENC.numberToWordArray(cipherKey);
    var encryptedText = CryptoJS.AES.encrypt(fileText, key, {iv: key});
    return encryptedText;
}

ENC.addUserId = function (text, userId) {
    return text + ENC.USER_ID_BEGIN + userId + ENC.USER_ID_END;
}

ENC.removeUserId = function (text) {
    return text.substring(0, text.lastIndexOf(ENC.USER_ID_BEGIN));
}

ENC.getSenderId = function (receivedText) {
    var startPos = receivedText.lastIndexOf(ENC.USER_ID_BEGIN);
    startPos += ENC.USER_ID_BEGIN.length;
    var endPos = receivedText.lastIndexOf(ENC.USER_ID_END);
    var senderId = receivedText.substring(startPos, endPos);
    return senderId;
}

ENC.decryptText = function (encryptedText, cipherKey) {
    var key = ENC.numberToWordArray(cipherKey);
    var decryptedText = CryptoJS.AES.decrypt(encryptedText, key, {iv: key});
    return decryptedText;
}
