﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    EDMS
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <div class="dashboard">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">

            <form name="search" action="<c:url value="/document"/>" method="post" enctype="charset=UTF-8">
                <input type="text" name="documentId" value="1" class="input-name"/>

                <div class="button_line">
                    <button type="submit" name="sendData" class="btn">Apply</button>
                </div>
            </form>

        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>