﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    EDMS - Send document
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">

            <div class="sendingPanel">
                <fieldset>
                    <legend>Send document</legend>

                    <label>Recipient</label>
                    <t:recipients/>

                    <label>File to sending</label>
                    <input type="file" id="fileToEncryption"/>

                    <label>File with private keys</label>
                    <input type="file" id="secretFile"/>

                    <label>Password for file with private keys</label>
                    <input type="password" class="sendPanelInput" id="filePassword" maxlength="25">

                    <div>
                        <input type="button" class="btn" name="encryptButton" value="Encrypt and Send"/>
                    </div>
                    <p>
                        <a id="decryptedFile"></a>
                    </p>
                </fieldset>
            </div>

        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>