﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    EDMS - Outbox
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">

            <div class="outboxPanel">
                <fieldset>
                    <legend>Outbox</legend>
                    <table class="table table-striped table-hover table-boarder">
                        <thead>
                        <tr class="primary-table">
                            <th class="item_title">#</th>
                            <th class="item_title">Recipient</th>
                            <th class="item_title">Document name</th>
                            <th class="item_title">Date</th>
                            <th class="item_title">Remove</th>
                        </tr>
                        </thead>
                        <c:if test="${fn:length(outboxDocuments) > 0}">
                            <c:forEach var="i" begin="0" step="1" end="${fn:length(outboxDocuments) - 1}">
                                <tr>
                                    <td>
                                        <div class="basket_field_value"><c:out value="${i + 1}"/></div>
                                    </td>
                                    <td>
                                        <div class="basket_field_value"><c:out value="${outboxDocuments[i].toParticipant.toParticipantString()}"/></div>
                                    </td>
                                    <td>
                                        <div class="basket_field_value"><c:out value="${outboxDocuments[i].documentName}"/></div>
                                    </td>
                                    <td>
                                        <fmt:formatDate value="${outboxDocuments[i].sendDate}" var="sendDate"
                                                        type="date" pattern="dd-MM-yyyy kk:mm:ss"/>
                                        <div class="basket_field_value"><c:out value="${sendDate}"/></div>
                                    </td>
                                    <td>
                                        <input type="button" class="removeButton btn" id="<c:out value="${outboxDocuments[i].id}"/>" value="Remove"/>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </fieldset>
            </div>

        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>