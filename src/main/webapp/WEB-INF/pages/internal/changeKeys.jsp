﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    EDMS - Change keys
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">

            <div class="inboxPanel">
                <fieldset>
                    <legend>Change keys</legend>

                    <div id="secretFilePanel">

                        <label>Password for file with private keys</label>
                        <input type="password" name="filePassword" maxlength="25">
                        <span class="help-block error" id="filePassword"></span>
                        <label>Repeat password for file with private keys</label>
                        <input type="password" name="filePasswordRepeat" maxlength="25">
                        <span class="help-block error" id="filePasswordRepeat"></span>

                        <p>
                            <button type="button" name="downloadSecretFile" class="btn">
                                Download file with keys
                            </button>
                        </p>
                    </div>

                    <input type="hidden" id="dhPublicKey" name="dhPublicKey">
                    <input type="hidden" id="curvePublicKey" name="curvePublicKey">

                    <p>
                        <a id="secretFile"></a>
                    </p>

                    <p>
                        <button name="changeKeys" class="btn invisible">
                            Change keys
                        </button>
                    </p>

                </fieldset>
            </div>

        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>