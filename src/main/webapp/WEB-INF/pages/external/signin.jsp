﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    EDMS - Login
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <div class="dashboard">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <fieldset>
                <legend>
                    Login
                </legend>
                <form name="mainLoginForm" action="<c:url value="/signin"/>"
                      method="post" class="form-inline">
                    <p>
                        <input type="text" name="formLoginEmail"
                               placeholder="Email" value="${email}">
                    </p>

                    <p>
                        <input type="password" name="formLoginPassword"
                               placeholder="Password">
                    </p>

                    <p>
                        <button type="submit" name="formSendLoginData" class="btn">
                            Login
                        </button>
                    </p>

                    <p>
                        <a href="<c:url value="/registration"/>"><button type="button" class="btn">Registration</button></a>
                    </p>

                </form>
                <span class="help-block error" id="formLoginError">${paramErrors['formLoginError'] }</span>
                <span class="help-block error" id="loginError">${paramErrors['loginError'] }</span>
            </fieldset>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>