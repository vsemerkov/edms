﻿<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title">
    EDMS - Registration
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<div class="container">
    <%@ include file="/WEB-INF/jspf/sign_in.jspf" %>
    <div class="dashboard">
        <%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="index_main_panel">
            <form name="signupForm" action="<c:url value="/registration"/>" method="post">
                <fieldset>
                    <legend>
                        Registration
                    </legend>
                    <label>First name</label>
                    <input type="text" id="inputError" name="firstName" maxlength="25" value="${formBean.firstName}">
                    <span class="help-block error" id="firstName">${paramErrors['firstName']}</span>
                    <label>Patronymic</label>
                    <input type="text" name="patronymic" maxlength="25" value="${formBean.patronymic}">
                    <span class="help-block error" id="patronymic">${paramErrors['patronymic']}</span>
                    <label>Last name</label>
                    <input type="text" name="lastName" maxlength="25" value="${formBean.lastName}">
                    <span class="help-block error" id="lastName">${paramErrors['lastName']}</span>
                    <label>Email</label>
                    <input type="text" name="email" maxlength="25" value="${formBean.email}">
                    <span class="help-block error" id="email">${paramErrors['email']}</span>
                    <label>Password</label>
                    <input type="password" name="password" maxlength="25">
                    <span class="help-block error" id="password">${paramErrors['password']}</span>
                    <label>Repeat password</label>
                    <input type="password" name="passwordRepeat" maxlength="25">
                    <span class="help-block error" id="passwordRepeat">${paramErrors['passwordRepeat']}</span>
                    <label>Enter the number shown in the image below</label>
                    <input type="text" id="inputError" name="captcha" maxlength="5">
                    <span class="help-block error" id="captcha">${paramErrors['captcha']}</span>
                    <t:captcha/>
                    <input type="hidden" id="dhPublicKey" name="dhPublicKey">
                    <span class="help-block error" id="passwordRepeat">${paramErrors['dhPublicKey']}</span>
                    <input type="hidden" id="curvePublicKey" name="curvePublicKey">
                    <span class="help-block error" id="passwordRepeat">${paramErrors['curvePublicKey']}</span>

                    <div id="secretFilePanel">
                        <label>Before registration you must download file with keys!</label>

                        <label>Password for file with private keys</label>
                        <input type="password" name="filePassword" maxlength="25">
                        <span class="help-block error" id="filePassword"></span>
                        <label>Repeat password for file with private keys</label>
                        <input type="password" name="filePasswordRepeat" maxlength="25">
                        <span class="help-block error" id="filePasswordRepeat"></span>

                        <p>
                            <button type="button" name="downloadSecretFile" class="btn">
                                Download file with keys
                            </button>
                        </p>
                    </div>

                    <p>
                        <a id="secretFile"></a>
                    </p>

                    <span class="help-block error" id="user">${paramErrors['user']}</span>

                    <p>
                        <button name="registration" class="btn invisible">
                            Registration
                        </button>
                    </p>
                </fieldset>
            </form>
        </div>
        <div class="btm"></div>
    </div>
    <%@ include file="/WEB-INF/jspf/footer.jspf" %>
</div>
</body>
</html>