<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setBundle basename="resources"/>

<c:choose>
	<c:when test="${empty user}">
	<form name="loginForm" action="<c:url value="/login"/>" method="post" class="form-inline">
  		<input type="text" name="loginEmail" placeholder="Email" value="${email}">
  		<input type="password" name="loginPassword" placeholder="Password">
  		<button type="submit" name="sendLoginData" class="btn">Login</button>
  		<a href="<c:url value="/registration"/>"><button type="button" class="btn">Registration</button></a>
	</form>
	<span class="help-block error" id="loginError">${paramErrors['loginError'] }</span>
	</c:when>
	<c:otherwise>
		<form name="logoutForm" action="<c:url value="/logout"/>" method="post" class="form-inline">
			<span class="user_email"><c:out value="${user.email}"/></span>
			<button type="submit" class="btn">Log out</button>
		</form>
	</c:otherwise>
</c:choose>