<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<select name="recipient" class="sendPanelInput">
    <option value="0"></option>
    <c:forEach var="recipient" items="${recipients}">
        <option value="${recipient.id}">
                ${recipient.toParticipantString()}
        </option>
    </c:forEach>
</select>