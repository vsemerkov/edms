<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<head>
    <title>${title}</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta name="author" content="Semerkov Vladimir"/>
    <meta name="keywords" content="EDMS"/>
    <meta name="description" content="EDMS"/>

    <link rel="icon" href="<c:url value="/img/favicon.ico"/>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<c:url value="/img/favicon.ico"/>" type="image/x-icon"/>

    <link rel="stylesheet" type="text/css" href="<c:url value="/style/jquery-ui.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/style/bootstrap.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/style/bootstrap-responsive.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/style/datepicker.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/style/styles.css"/>"/>

    <script type="text/javascript" src="<c:url value="/js/jquery/jquery-1.9.1.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/jquery/jquery-ui.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/bootstrap/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/bootstrap/bootstrap-dropdown.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/bootstrap/bootstrap-datepicker.js"/>"></script>

    <!-- for pkcs5pkey -->
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/core.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/enc-base64.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/md5.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/evpkdf.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/cipher-core.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/tripledes.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/enc-utf16.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/aes.js"/>"></script>

    <!-- for crypto -->
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/sha1.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/cryptojs/components/sha256.js"/>"></script>

    <!-- for crypto, asn1, asn1x509 -->
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/yahoo.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/jsbn.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/jsbn2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/prng4.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/rng.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/rsa.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/rsa2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/base64.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/sha256.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/ec.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ext/ec-patch.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ecdsa-modified-1.0.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/ecparam-1.0.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/asn1hex-1.1.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/base64x-1.1.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/rsapem-1.1.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/rsasign-1.2.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/x509-1.1.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/pkcs5pkey-1.0.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/asn1-1.0.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/asn1x509-1.0.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/dsa-modified-1.0.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/keyutil-1.0.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/kjur-jsrsasign-4.9.0-0/crypto-1.1.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/util.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/rsa.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/curve.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/DH.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/secretFile.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/registration.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/enc.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/documentSending.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/changeKeys.js"/>"></script>
</head>